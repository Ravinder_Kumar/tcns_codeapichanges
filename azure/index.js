const _config = require("../config");
const azure = require("azure-storage");

let connectionString = _config.azureConnectionString;

const queueSvc = azure.createQueueService(connectionString);
const blobService = azure.createBlobService(connectionString);

class Azure {
  
  async fileBlob(blobName, stream, streamLength, callback) {
      console.log('Uploading...');
      
    let promise = new Promise(async (resolve, reject) => {
      let blob = blobService.createBlockBlobFromStream(
        "tcnsax",
        blobName,
        stream,
        streamLength,
        async err => {
          if (err) {
            console.log(err);
            callback(false);
            return false;
          }
          let a = callback(true);
          console.log("File Uploaded!", blobName);
          //return true;

          resolve(a);
        }
      );
    });
    return promise;
  }
}

module.exports = Azure;
