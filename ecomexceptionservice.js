const soapRequest = require("easy-soap-request");
const fs = require("fs");
const http = require("https");
const parseString = require("xml2js").parseString;
const xml2js = require("xml2js");
const express = require("express");
const fileUpload = require("express-fileupload");
const request = require('request');
const app = express();
const path = require("path");
const getStream = require("into-stream");
const Azure = require("./azure");
const azure = new Azure();
var streamLength = require("stream-length");
const csvjson = require("csvjson");
const createReadStream = require("fs").createReadStream;
const createWriteStream = require("fs").createWriteStream;
const sqlConnection = require("mssql");
const _config = require("./config");
const axios = require("axios");
var convert = require("xml-js");
var TSV = require("tsv");
PSV = new TSV.Parser("\t");
const { google } = require("googleapis");
const h2p = require("html2plaintext");
const replace = require('replace-in-file');
const bodyparser = require('body-parser')
const moment = require('moment');
app.use(bodyparser.json())
const mailer=require('./mail/mailer')
// default options
app.use(fileUpload());

var batchid = moment().format("YYYYMMDDHHMMSS"); 

const sleep = require('sleep-promise');
const fetch = require('node-fetch');

module.exports = {
     eComAndDelhivery: async (type) => {
      console.log(_config.dbConfig);
      let connection = await sqlConnection.connect(_config.dbConfig);
      console.log("connected");

      let page = 1
      let pages ;

      let rowsOfPage = 20;

      var request = await connection.request();
    
      request.input('number_of_rows', sqlConnection.Int, rowsOfPage ) 

      request.execute('sp_total_page_count' ,(err,res)=>{

         pages =  res.recordset[0].TotalPagesCount

         console.log('Pages -------->',pages);
        
       while(pages >= page){
  
          try {
   
            if(type == 'eCom'){
  
              let result = []
  
             var request =   connection.request();
             request.input('PageNumber', sqlConnection.Int, page)
             request.input('RowsOfPage', sqlConnection.Int, rowsOfPage ) 
    
             request.execute('sp_ecom_pagination' ,(err,res)=>{

                    res.recordset.forEach(item =>{

                      result.push(item)
              }) 

             getDataFromApi(result,type)
      
                })
            }

        }
         catch (error) {
          let obj={
            functionaName:"Ecomm DB Connection Error",
            error:error,
            stringError:JSON.stringify(error,undefined,4)
           }
          //  mailer.main(obj);
          console.log(error);  
        }

          page++
    
     
      }
    })      
  }  ,
    
     eComAndDelhiveryException: async(type)=>{

      myFinalArray = []

      let connection = await sqlConnection.connect(_config.dbConfig);

      result = await connection
       .request()
       .query(
        "SELECT  awb_number,XML_response,batchid from [ecom_and_scan_stages_Exception]"
         );

       //  console.log('Final Result ------------>',result.recordset);

         result.recordset.forEach(item => {
          myFinalArray.push(item)
         })

         getExceptionData(myFinalArray,type)

         console.log('Inside this function');
    }
}

let count = 0
    
async function insertingData (trackingNo , type ) {

  let connection = await sqlConnection.connect(_config.dbConfig);

  let d = new Date()

    console.log(`Tracking No ------>,${trackingNo} , Ecom Start Time ---------->, ${d}`);

    let newTrackingNumber = trackingNo

    var url =
    "https://plapi.ecomexpress.in/track_me/api/mawbd/?password=WJu47WgNg5mBR35K&username=tcnsclothing112200_women&awb=" +
    trackingNo;

    let response = await fetch(url);
    
    const responseText = await response.text();

    // start re-try concept

    if(responseText.message == 'Internal server error' || responseText.message == 'Endpoint request timed out'){
    
    if(count <= 3){
      setTimeout(()=>{
        insertingData(newTrackingNumber,type)
        count++ ;
      },2000)
    }
    else{

      let result = await connection
      .request().input('trackingNo', sqlConnection.NVarChar, trackingNo).input('resp', sqlConnection.NVarChar, responseText).input('batchid', sqlConnection.NVarChar, batchid)
      .query(
        "INSERT INTO ecom_and_scan_stages_Exception (awb_number, xml_response, batchid) VALUES (@trackingNo ,@resp, @batchid)"
      )
    }

    // end re-try concept
    
    }else{
    
      parseString(responseText, function(error,result) {
        if(error){

          let result =  connection
          .request().input('trackingNo', sqlConnection.NVarChar, trackingNo).input('resp', sqlConnection.NVarChar, responseText).input('batchid', sqlConnection.NVarChar, batchid)
          .query(
            "INSERT INTO ecom_and_scan_stages_Exception (awb_number, xml_response, batchid) VALUES (@trackingNo ,@resp, @batchid)"
          )
            // console.log("Error in xml ---->",error)
            // return
        }
        else{
        
            let dataArray  =  result['ecomexpress-objects'].object[0].field

            let dbData = []
            let db =[]
            let newObj = {}
    
            for(i=0;i<dataArray.length;i++){
    
            if(dataArray[i].$.name != 'scans'){
                  dbData.push({name : dataArray[i].$.name, value : dataArray[i]._ ? dataArray[i]._  : null}) 
            }
    
            var mapped =  dbData.map(item => ({ [item.name]: item.value }) );
             newObj = Object.assign({}, ...mapped );

             
             if(dataArray[i].$.name == 'scans'){
                let data = result['ecomexpress-objects'].object[0].field
                    for (const iterator of data) {
                        if(iterator.$.name == 'scans'){
                            iterator.object.forEach(item => {
    
                             let jsonObj = {}
    
                             for(let k=0; k<item.field.length;k++){
    
                               jsonObj['awb_number'] = newObj.awb_number
                               jsonObj['orderid'] = newObj.orderid
                               jsonObj['pickupdate'] = newObj.pickupdate
                               let name = item.field[k].$.name
                               jsonObj[name] = item.field[k]._ ? item.field[k]._ : null
                               
                                }
                                db.push(jsonObj)                      
                            });
                        }
                    }
                    }      
                  }


          
               var request =   connection.request();

                request.input('awb_number', sqlConnection.NVarChar(255), newObj.awb_number)
                request.input('orderid', sqlConnection.NVarChar(255), newObj.orderid ) 
                request.input('actual_weight', sqlConnection.Float, newObj.actual_weight)
                request.input('origin', sqlConnection.NVarChar(255), newObj.origin)
                request.input('destination', sqlConnection.NVarChar(255), newObj.destination)
                request.input('current_location_name', sqlConnection.NVarChar(255), newObj.current_location_name)
                request.input('current_location_code', sqlConnection.NVarChar(255), newObj.current_location_code)
                request.input('customer', sqlConnection.NVarChar(255), newObj.customer)
                request.input('consignee', sqlConnection.NVarChar(255), newObj.consignee)
                request.input('pickupdate', sqlConnection.DateTime, newObj.pickupdate)
                request.input('status', sqlConnection.NVarChar(255), newObj.status)
                request.input('tracking_status', sqlConnection.NVarChar(255), newObj.tracking_status)
                request.input('reason_code', sqlConnection.NVarChar(255), newObj.reason_code)
                request.input('reason_code_number', sqlConnection.NVarChar(255), newObj.reason_code_number)
                request.input('reason_code_description', sqlConnection.NVarChar(255), newObj.reason_code_description)
                request.input('receiver', sqlConnection.NVarChar(255), newObj.receiver)
                request.input('lat', sqlConnection.Float, newObj.lat)
                request.input('long', sqlConnection.Float, newObj.long)
                request.input('expected_date', sqlConnection.DateTime, newObj.expected_date)
                request.input('last_update_date', sqlConnection.DateTime, newObj.last_update_date ) 
                request.input('last_update_datetime', sqlConnection.DateTime, newObj.last_update_datetime)
                request.input('ref_awb', sqlConnection.NVarChar(255), newObj.ref_awb)
                request.input('delivery_date', sqlConnection.DateTime, newObj.delivery_date)
                request.input('rts_shipment', sqlConnection.Int, newObj.rts_shipment)
                request.input('system_delivery_update', sqlConnection.DateTime, newObj.system_delivery_update)                    
                request.input('rts_system_delivery_status', sqlConnection.NVarChar(255), newObj.rts_system_delivery_status)
                request.input('rts_reason_code_number', sqlConnection.NVarChar(255), newObj.rts_reason_code_number)
                request.input('rts_last_update', sqlConnection.DateTime, newObj.rts_last_update )
                request.input('pincode', sqlConnection.Int, newObj.pincode)
                request.input('city', sqlConnection.NVarChar(255), newObj.city)
                request.input('state', sqlConnection.NVarChar(255), newObj.state ) 
                request.input('delivery_pod_image', sqlConnection.NVarChar(255), newObj.delivery_pod_image)
                request.input('delivery_pod_signature', sqlConnection.NVarChar(255), newObj.delivery_pod_signature)
                request.input('rev_pickup_signature', sqlConnection.NVarChar(255), newObj.rev_pickup_signature)
                request.input('rev_pickup_packed_image', sqlConnection.NVarChar(255), newObj.rev_pickup_packed_image)
                request.input('rev_pickup_open_image', sqlConnection.NVarChar(255), newObj.rev_pickup_open_image)

                
                request.execute('sp_new_ecom',(err,result)=>{

                  console.log('Ecom Done ------------->');

                  let endDate = new Date();

                  console.log(`Tracking No ------>,${trackingNo} , Ecom End Date Time ---------->, ${endDate}`);

                   EcomScansDetail(db);
                })


                  if(type == 'eComMissing'){
                    let resultdata = connection
                        .request()
                        //.input('awb_number', sqlConnection.NVarChar, awb_number).input('resp', sqlConnection.NVarChar, responseText).input('batchid', sqlConnection.NVarChar, batchid)
                        .query(
                          `delete from ecom_and_scan_stages_Exception where awb_number= '${trackingNo}'`
                          );        
                }
      }})

    }
    
    }
    
async function EcomScansDetail(db){


      let connection = await sqlConnection.connect(_config.dbConfig);

      let ecomScansObj = {}

        db.forEach(item => {

        ecomScansObj = {}

        ecomScansObj['awb_number'] = item.awb_number
        ecomScansObj['pickupdate'] = item.pickupdate 
        ecomScansObj['orderid'] = item.orderid
        ecomScansObj['updated_on'] = item.updated_on 
        ecomScansObj['status'] = item.status
        ecomScansObj['reason_code'] = item.reason_code
        ecomScansObj['reason_code_number'] = item.reason_code_number
        ecomScansObj['scan_status'] = item.scan_status
        ecomScansObj['call_attempted'] = item.call_attempted
        ecomScansObj['call_duration'] = item.call_duration 
        ecomScansObj['call_log_duration'] = item.call_log_duration
        ecomScansObj['call_start_time'] = item.call_start_time
        ecomScansObj['location'] = item.location 
        ecomScansObj['location_city'] = item.location_city 
        ecomScansObj['location_type'] = item.location_type 
        ecomScansObj['city_name'] = item.city_name
        ecomScansObj['Employee'] = item.Employee 
        
        
        var request =   connection.request();

        request.input('awb_number', sqlConnection.NVarChar(255), ecomScansObj.awb_number)
        request.input('pickupdate', sqlConnection.DateTime, ecomScansObj.pickupdate)
        request.input('orderid', sqlConnection.NVarChar(255), ecomScansObj.orderid ) 
        request.input('updated_on', sqlConnection.DateTime, ecomScansObj.updated_on)
        request.input('status', sqlConnection.NVarChar(255), ecomScansObj.status)
        request.input('reason_code', sqlConnection.NVarChar(255), ecomScansObj.reason_code)
        request.input('reason_code_number', sqlConnection.NVarChar(255), ecomScansObj.reason_code_number)
        request.input('scan_status', sqlConnection.NVarChar(255), ecomScansObj.scan_status)
        request.input('call_attempted', sqlConnection.NVarChar(255), ecomScansObj.call_attempted)
        request.input('call_duration', sqlConnection.NVarChar(255), ecomScansObj.call_duration)
        request.input('call_log_duration', sqlConnection.NVarChar(255), ecomScansObj.call_log_duration)
        request.input('call_start_time', sqlConnection.NVarChar(255), ecomScansObj.call_start_time ) 
        request.input('location', sqlConnection.NVarChar(255), ecomScansObj.location)
        request.input('location_city', sqlConnection.NVarChar(255), ecomScansObj.location_city)
        request.input('location_type', sqlConnection.NVarChar(255), ecomScansObj.location_type)
        request.input('city_name', sqlConnection.NVarChar(255), ecomScansObj.city_name)
        request.input('Employee', sqlConnection.NVarChar(255), ecomScansObj.Employee)

        let ScanStartDate = new Date();

        console.log(`Tracking No ------>,${ecomScansObj.awb_number} , Scan Stages Start Date Time ---------->, ${ScanStartDate}`);
       
              request.execute('sp_new_ecom_scan_stages',(err,result)=>{

                let ScanStagesEndDate = new Date();

                console.log(`Tracking No ------>,${ecomScansObj.awb_number} , Scan Stages End Date Time ---------->, ${ScanStagesEndDate}`);
               
              })   
        })
  }

async function getDataFromApi(result ,type ){

    dataArray = result;

    let eComArray = [];

    if(type == 'eCom'){
      
      dataArray.forEach(element => {
       if (element.Channel_Name.toLowerCase() == "wcustom", "storehippo") {

       // storehippo
      
      if (element.Shipping_provider) {
        if (
          element.Shipping_provider.toLowerCase() ==
          "ecomexpress-w and wishful"
        ) {
          eComArray.push(element);
        }
      }
      if (element.Shipping_provider) {
        if (
          element.Shipping_provider.toLowerCase() ==
          "ecomExpress-aurelia"
        ) {
          eComArray.push(element);
        }
      }
      
    }
    });
    }
 
  console.log(eComArray.length, "eComArray");

  let innerApiCalls = []

  if (eComArray.length > 0) {
          
    // for(let index=0;index<eComArray.length;index++){

      eComArray.forEach(item=>{

        if(type == 'eCom'){
          //element = eComArray[index];
   
          let trackingNo = item.Tracking_Number;
   
           trackingNo = Number.parseFloat(trackingNo).toPrecision();
   
           innerApiCalls.push(insertingData(trackingNo, type))
    
        }

      })
    
    
   
  //  }
    
 }  


if(type == 'eCom'){
  await Promise.all(innerApiCalls)
}

  }

  function getExceptionData(result,type) {

    let eComArray =[]
    dataArray = result;


    dataArray.forEach(element => {
        eComArray.push(element);
    });

    eComArray.forEach(item =>{
      insertingData(item.awb_number,type)
    })

  




  //   for(let index=0;index<eComArray.length;index++){

  //     element = eComArray[index];
  //    let trackingNo = element.awb_number;
  //    trackingNo = Number.parseFloat(trackingNo).toPrecision();
   
  //    insertingData(trackingNo, type);

  // }
  }

