const soapRequest = require("easy-soap-request");
const fs = require("fs");
const http = require("https");
const parseString = require("xml2js").parseString;
const xml2js = require("xml2js");
const express = require("express");
const fileUpload = require("express-fileupload");
const app = express();
const path = require("path");
const getStream = require("into-stream");
const Azure = require("./azure");
const azure = new Azure();
var streamLength = require("stream-length");
const csvjson = require("csvjson");
const createReadStream = require("fs").createReadStream;
const createWriteStream = require("fs").createWriteStream;
const sql = require("mssql");
const _config = require("./config");
const axios = require("axios");
var convert = require("xml-js");
var TSV = require("tsv");
PSV = new TSV.Parser("\t");
const { google } = require("googleapis");
const h2p = require("html2plaintext");
const replace = require('replace-in-file');
const moment = require('moment');
const mailer=require('./mail/mailer')
// default options
app.use(fileUpload());
var jobCode;
var filePath;
var jobCode30;
var filePath30;
var batchid = moment().format("YYYYMMDDHHMMSS"); 
const sampleHeaders = {
  "user-agent": "sampleTest",
  "Content-Type": "text/xml;charset=UTF-8",
  soapAction: ""
};
const sleep = require('sleep-promise');
const fetch = require('node-fetch');
// usage oef module
module.exports = {
  callSoap: async type => {
    console.log(moment().format(),"EXecution Start DateTime");
    let url = "https://tcns.unicommerce.com/services/soap/uniware19.wsdl";
     //New Changes
     let dataurl = "https://tcns.unicommerce.com:443/services/soap/?version=1.9&facility=glaucus";
     let dataurlTCNS_113 = "https://tcns.unicommerce.com:443/services/soap/?version=1.9&facility=TCNS_113";
     let dataurlGR_GL_01 = "https://tcns.unicommerce.com:443/services/soap/?version=1.9&facility=GR_GL_01";
     //End New Changess
    let xml = fs.readFileSync("xmlbody/" + type + ".xml", "utf-8");//readiing body for the soap request
    let xml30 = fs.readFileSync("xmlbody/" + type + ".xml", "utf-8"); //xml30 for 30 minutes interval
  //New Changes 
  let xmlshipping = fs.readFileSync("xmlbody/" + type + ".xml", "utf-8");
  let xmlshipping45dayback = fs.readFileSync("xmlbody/" + type + ".xml", "utf-8");
  let xmlsaleorder30day = fs.readFileSync("xmlbody/" + type + ".xml", "utf-8");
  let xmlsaleorder30dayback = fs.readFileSync("xmlbody/" + type + ".xml", "utf-8");

  //End New Changes 
    console.log("start");
    console.log(type,"TYPE");
    // console.log("type");
    
    var startDate = new Date();
    startHours = startDate.getUTCHours();
    startMinutes = startDate.getUTCMinutes();
    startSeconds = startDate.getUTCSeconds();
    startHours = ("0" + startHours).slice(-2);
    startMinutes = ("0" + startMinutes).slice(-2);
    startSeconds = ("0" + startSeconds).slice(-2);

    var endDate = new Date();
    endHours = endDate.getUTCHours();
    endMinutes = endDate.getUTCMinutes();
    endSeconds = endDate.getUTCSeconds();
    endHours = ("0" + endHours).slice(-2);
    endMinutes = ("0" + endMinutes).slice(-2);
    endSeconds = ("0" + endSeconds).slice(-2);

    startDate.setDate(endDate.getDate() - 60);
    var start =
      startDate.getUTCFullYear() +
      "-" +
      ("0" + (startDate.getUTCMonth() + 1)).slice(-2) +
      "-" +
      ("0" + (startDate.getUTCDate() + 1)).slice(-2) +
      "T" +
      startHours +
      ":" +
      startMinutes +
      ":" +
      startSeconds +
      "Z"; // 00:00:00Z';
    var end =
      endDate.getUTCFullYear() +
      "-" +
      ("0" + (endDate.getUTCMonth() + 1)).slice(-2) +
      "-" +
      ("0" + (endDate.getUTCDate() + 1)).slice(-2) +
      "T" +
      endHours +
      ":" +
      endMinutes +
      ":" +
      endSeconds +
      "Z"; // 00:00:00Z';
    console.log(start);
    console.log(end);
    var test = moment().format().split('+')[0] + 'Z';
    var test2 = moment().subtract(60, 'days').format().split('+')[0] + 'Z';
    var saleorder30day = moment().subtract(30, 'days').format().split('+')[0] + 'Z';
    //New Changes
    var test3 = moment().subtract(90, 'days').format().split('+')[0] + 'Z';
    //End New Changes
    var test30 = moment().subtract(1, 'days').format().split('+')[0] + 'Z'; //test30 for calulating 1 days 
    console.log(test);
    console.log(test2);
    endDate.getUTCDate();
    //New Changes
    let yesterday=moment(new Date()).subtract(1,'days').format().split('+')[0] + 'Z';
    let back30day=moment(new Date()).subtract(30,'days').format().split('+')[0] + 'Z';
    let shippingback45day=moment(new Date()).subtract(45,'days').format().split('+')[0] + 'Z';
    //console.log(yesterday,"yesterday");
    //End New Changes
    xml = xml.replace("@startdate", test2);
    xml = xml.replace("@enddate", test);
    xml30 = xml30.replace("@startdate",test30)
    xml30 = xml30.replace("@enddate", test);
    //New Changes
    // xmlshipping = xmlshipping.replace("@startdate", test3);
    // xmlshipping = xmlshipping.replace("@enddate", yesterday);
    xmlshipping = xmlshipping.replace("@startdate", shippingback45day);
    xmlshipping = xmlshipping.replace("@enddate", yesterday);
    xmlsaleorder30day = xmlsaleorder30day.replace("@startdate", saleorder30day);
    xmlsaleorder30day = xmlsaleorder30day.replace("@enddate", test);
    xmlsaleorder30dayback = xmlsaleorder30dayback.replace("@startdate", test2);
    xmlsaleorder30dayback = xmlsaleorder30dayback.replace("@enddate", back30day);

    xmlshipping45dayback = xmlshipping45dayback.replace("@startdate", test3);
    xmlshipping45dayback = xmlshipping45dayback.replace("@enddate", shippingback45day);
    //End New Changes

    //console.log(xml);

    // calling the soap api
    try {
      
      if(type=="saleorder")
      {
        
        let { response } = await soapRequest({
          url: url,
          headers: sampleHeaders,
          xml: xmlsaleorder30day
        });
    let { headers, body, statusCode } = response;
    await parseString(body, function (err, result) {
      jobCode =
        result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
          "CreateExportJobResponse"
        ][0].JobCode[0];
    });
   // parsing the xml to json and initializing the job code

   console.log(headers);
  

    await getJobBody(type);
      }

      if(type=="saleorder1")
      {
        
        let { response } = await soapRequest({
          url: url,
          headers: sampleHeaders,
          xml: xmlsaleorder30dayback
        });
    let { headers, body, statusCode } = response;
    await parseString(body, function (err, result) {
      jobCode =
        result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
          "CreateExportJobResponse"
        ][0].JobCode[0];
    });
   // parsing the xml to json and initializing the job code

   console.log(headers);
  

    await getJobBody(type);
      }
  
    // console.log(response);

    // let { headers, body, statusCode } = response;
      if(type=="saleorder30")
      {
        let { response } = await soapRequest({
          url: url,
          headers: sampleHeaders,
          xml: xml30
        });
    let { headers, body, statusCode } = response;
    await parseString(body, function (err, result) {
      jobCode30 =
        result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
          "CreateExportJobResponse"
        ][0].JobCode[0];
    });

    await getJobBody(type);

      }
      if(type=="reversepickup")
      {
        
        let { response } = await soapRequest({
          url: url,
          headers: sampleHeaders,
          xml: xml
        });
    let { headers, body, statusCode } = response;
    await parseString(body, function (err, result) {
      jobCode =
        result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
          "CreateExportJobResponse"
        ][0].JobCode[0];
    });
   // parsing the xml to json and initializing the job code

  //  console.log(headers);
  

    await getJobBody(type);
      }
      if(type=="inventorysnapshot")
      {
        
        let { response } = await soapRequest({
          url: url,
          headers: sampleHeaders,
          xml: xml
        });
    let { headers, body, statusCode } = response;
    await parseString(body, function (err, result) {
      jobCode =
        result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
          "CreateExportJobResponse"
        ][0].JobCode[0];
    });
   // parsing the xml to json and initializing the job code

  //  console.log(headers);
  

    await getJobBody(type);
      }
      //New Changes
      if (type == "shippingpackagetimeline_glaucus")
      //if(type=="Shipping_package_timeline")
      {

        let { response } = await soapRequest({
          url: dataurl,
          headers: sampleHeaders,
          xml: xmlshipping
        });
        // console.log(response);
        let { headers, body, statusCode } = response;
        await parseString(body, function (err, result) {
          jobCode =
            result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
              "CreateExportJobResponse"
            ][0].JobCode[0];
        });
        await getJobBody(type);
      }
      //New...........................
      if (type == "shippingpackagetimeline_glaucus_1")
      {

        let { response } = await soapRequest({
          url: dataurl,
          headers: sampleHeaders,
          xml: xmlshipping45dayback
        });
        let { headers, body, statusCode } = response;
        await parseString(body, function (err, result) {
          jobCode =
            result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
              "CreateExportJobResponse"
            ][0].JobCode[0];
        });
        await getJobBody(type);
      }
      //...............................
      if (type == "shippingpackagetimeline_TCNS_113")
      {

        let { response } = await soapRequest({
          url: dataurlTCNS_113,
          headers: sampleHeaders,
          xml: xmlshipping
        });
        console.log(response);
        let { headers, body, statusCode } = response;
        await parseString(body, function (err, result) {
          jobCode =
            result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
              "CreateExportJobResponse"
            ][0].JobCode[0];
        });
        await getJobBody(type);
      }
      //New...................
      if (type == "shippingpackagetimeline_TCNS_113_1")
      {

        let { response } = await soapRequest({
          url: dataurlTCNS_113,
          headers: sampleHeaders,
          xml: xmlshipping45dayback
        });
        console.log(response);
        let { headers, body, statusCode } = response;
        await parseString(body, function (err, result) {
          jobCode =
            result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
              "CreateExportJobResponse"
            ][0].JobCode[0];
        });
        await getJobBody(type);
      }
      //..............................
      if (type == "shippingpackagetimeline_GR_GL_01")
      {

        let { response } = await soapRequest({
          url: dataurlGR_GL_01,
          headers: sampleHeaders,
          xml: xmlshipping
        });
        console.log(response);
        let { headers, body, statusCode } = response;
        await parseString(body, function (err, result) {
          jobCode =
            result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
              "CreateExportJobResponse"
            ][0].JobCode[0];
        });
        await getJobBody(type);
      }
      //New................
      if (type == "shippingpackagetimeline_GR_GL_01_1")
      {

        let { response } = await soapRequest({
          url: dataurlGR_GL_01,
          headers: sampleHeaders,
          xml: xmlshipping45dayback
        });
        console.log(response);
        let { headers, body, statusCode } = response;
        await parseString(body, function (err, result) {
          jobCode =
            result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
              "CreateExportJobResponse"
            ][0].JobCode[0];
        });
        await getJobBody(type);
      }
      //End New Changes
    // // console.log(response);


    // parsing the xml to json and initializing the job code
   
  } catch (error) {
    let obj={
      functionaName:"Unicommerce API Error",
      error:error,
      stringError:JSON.stringify(error,undefined,4)
     }
     mailer.main(obj)
     console.log(error,"error")
     console.log(JSON.stringify(error),"stringyf");
  }
  },


  eComAndDelhivery: async () => {
    console.log("inside ecom");
    console.log(_config.dbConfig);
    let connection = await sql.connect(_config.dbConfig);
    console.log("connected");

    try {
      // let result = await connection
      // .request()
      // .query(
      //   " select Channel_Name,Tracking_Number,Reverse_Tracking_Number,Shipping_provider,Reverse_Shipping_provider, created,sale_order_item_status from [unicommerce_sale] where DATEDIFF(day,created,(GETDATE()-1))>-1 and DATEDIFF(day,created,(GETDATE()-1))<30   and channel_name = 'WCUSTOM' and ( shipping_provider ='Delhivery-wforwoman' or reverse_shipping_provider ='Delhivery-wforwoman' or  shipping_provider ='EcomExpress-W and Wishful' or  shipping_provider ='EcomExpress-Aurelia' or reverse_shipping_provider ='EcomExpress-W and Wishful' or  reverse_shipping_provider ='EcomExpress-Aurelia' or shipping_provider ='Shadowfax' or reverse_shipping_provider ='Shadowfax') and sale_order_item_status <>'DELIVERED' and sale_order_item_status <>'CANCELLED' or ( DATEDIFF(day,created,getDate())=1 and channel_name = 'WCUSTOM' and ( shipping_provider ='Delhivery-wforwoman' or reverse_shipping_provider ='Delhivery-wforwoman' or shipping_provider ='EcomExpress-W and Wishful' or  shipping_provider ='EcomExpress-Aurelia' or   reverse_shipping_provider ='EcomExpress-W and Wishful' or  reverse_shipping_provider ='EcomExpress-Aurelia' or  shipping_provider ='Shadowfax' or reverse_shipping_provider ='Shadowfax')) "
      // );
       let result = await connection
      .request()
      .query(
        //" select distinct Channel_Name,Tracking_Number,Reverse_Tracking_Number,Shipping_provider,Reverse_Shipping_provider, created,sale_order_item_status , updated from [unicommerce_sale] where  (DATEDIFF(day,updated,getDate())=1 and channel_name = 'WCUSTOM' and ( shipping_provider ='Delhivery-wforwoman' or reverse_shipping_provider ='Delhivery-wforwoman' or  shipping_provider ='EcomExpress-W and Wishful' or  shipping_provider ='EcomExpress-Aurelia' or   reverse_shipping_provider ='EcomExpress-W and Wishful' or  reverse_shipping_provider ='EcomExpress-Aurelia' or shipping_provider ='Shadowfax' or reverse_shipping_provider ='Shadowfax') and created < updated)"
        
        
        //"SELECT  DISTINCT Tracking_Number,Channel_Name,Reverse_Tracking_Number, Shipping_provider, Reverse_Shipping_provider, created,sale_order_item_status , updated from    [unicommerce_sale] where   channel_name = 'WCUSTOM'  AND (shipping_provider IN ('Delhivery-wforwoman', 'EcomExpress-W and wishful', 'EcomExpress-Aurelia', 'Shadowfax') OR reverse_shipping_provider IN ('Delhivery-wforwoman', 'EcomExpress-W and Wishful', 'EcomExpress-Aurelia', 'Shadowfax') ) AND (Datediff(day,updated,Getdate())=1 OR ((created > GETDATE()-60) AND Shipping_Package_Status_Code NOT IN ('DELIVERED', 'RETURNED')))"
        //"select DISTINCT Tracking_Number,Channel_Name,Reverse_Tracking_Number, Shipping_provider, Reverse_Shipping_provider, created,sale_order_item_status , updated from [unicommerce_sale] where created between cast('2021-03-18'as  date) and cast('2021-03-19'as date) and channel_name in ('wcustom','storehippo') AND (shipping_provider IN ('Delhivery-wforwoman') OR reverse_shipping_provider IN ('Delhivery-wforwoman') )"
        //"SELECT  DISTINCT Tracking_Number,Channel_Name,Reverse_Tracking_Number, Shipping_provider, Reverse_Shipping_provider,created,sale_order_item_status , updated from [unicommerce_sale] where created between cast('2021-01-01'as  date) and cast('2021-01-31'as date) and channel_name = 'STOREHIPPO' AND (shipping_provider IN ('Shadowfax') OR reverse_shipping_provider IN ('Shadowfax') )"

        " SELECT  DISTINCT Tracking_Number,Channel_Name,Reverse_Tracking_Number, Shipping_provider, Reverse_Shipping_provider, created,sale_order_item_status , updated from [unicommerce_sale] where created between cast('2021-03-01'as  date) and cast('2021-03-26'as date) and channel_name = 'WCUSTOM' AND (shipping_provider IN ('EcomExpress-W and wishful', 'EcomExpress-Aurelia') OR reverse_shipping_provider IN ('EcomExpress-W and Wishful', 'EcomExpress-Aurelia') )"
        );
   
    console.log("req completed db");
        // console.log(result.recordset);
        
    dataArray = result.recordset;
    // let eComArray = [];
    // let ecomReverseArray = [];
    let delhiveryArray = [];
    let delhiveryReverseArray = [];
    let finalDelhiveryArray = [];
    let shadowfaxArray = [];
    let reverseShadowfaxArray = [];
    let finalShadowFaxArray = [];
    let finalReverseShadowFaxArray = [];
    dataArray.forEach(element => {
      if (element.Channel_Name.toLowerCase() == "wcustom","storehippo") {
      //if (element.Channel_Name.toLowerCase() == "storehippo") {
        
        if (element.Shipping_provider) {
          if (
            element.Shipping_provider.toLowerCase() == "delhivery-wforwoman"
          ) {
            //console.log(element,"pushing in delhivery");
            delhiveryArray.push(element);
          }
        }
      //   // console.log('out')
        // if (element.Shipping_provider) {
        //   if (
        //     element.Shipping_provider.toLowerCase() ==
        //     "ecomexpress-w and wishful"
        //   ) {
        //     eComArray.push(element);
        //   }
        // }
        // if (element.Shipping_provider) {
        //   if (
        //     element.Shipping_provider.toLowerCase() ==
        //     "ecomExpress-aurelia"
        //   ) {
        //     eComArray.push(element);
        //   }
        // }
        if (element.Shipping_provider) {
          if (
            element.Shipping_provider.toLowerCase() ==
            "shadowfax"
          ) {
            shadowfaxArray.push(element);
          }
        }
        if (element.Reverse_Shipping_provider) {
          if (
            element.Reverse_Shipping_provider.toLowerCase() ==
            "shadowfax"
          ) {
            reverseShadowfaxArray.push(element);
          }
        }
        if (element.Reverse_Shipping_provider) {
          if (
            element.Reverse_Shipping_provider.toLowerCase() ==
            "delhivery-wforwoman"
          ) {
            delhiveryReverseArray.push(element);
          }
        }
        // if (element.Reverse_Shipping_provider) {
        //   if (
        //     element.Reverse_Shipping_provider.toLowerCase() ==
        //     "ecomexpress-w and wishful"
        //   ) {
        //     eComArray.push(element);
        //   }
        // }
        // if (element.Reverse_Shipping_provider) {
        //   if (
        //     element.Reverse_Shipping_provider.toLowerCase() ==
        //     "ecomExpress-aurelia"
        //   ) {
        //     eComArray.push(element);
        //   }
        // }
      }
    });
  //  console.log(eComArray.length, "ecomArray");
    console.log(delhiveryArray.length, "delhivery array");
    console.log(shadowfaxArray.length, "shadowfaxArray");
    console.log(reverseShadowfaxArray.length, "reverseShadowfaxArray");
    console.log(delhiveryReverseArray.length, "delhiveryReverse array");
   // console.log(ecomReverseArray.length, "ecomReverse array");
     
    //eCom++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // if (eComArray.length > 0) {
    //   console.log("inside ecom");

    //   //let truncateTable = await connection.query("Truncate table ecom_and_scan_stages_from_api");

    //   for(let index=0;index<eComArray.length;index++)
    //   {
    //     // console.log(eComArray[index]);
    //     element = eComArray[index];
    //     let trackingNo = element.Tracking_Number;
    //     // console.log(Number.parseFloat(trackingNo).toPrecision());
    //     trackingNo = Number.parseFloat(trackingNo).toPrecision();
    //     console.log(index);
        
    //       var url =
    //         "https://plapi.ecomexpress.in/track_me/api/mawbd/?password=WJu47WgNg5mBR35K&username=tcnsclothing112200_women&awb=" +
    //         trackingNo;
    //         let response = await fetch(url);
    //         const responseText = await response.text();
    //         // console.log(typeof json);

    //         console.log('Tracking Number --------->',trackingNo);
    //         //console.log('responseText --------->',responseText);

    //         if(responseText.length > 0){
    //           let result = await connection
    //           .request().input('trackingNo', sql.NVarChar, trackingNo).input('resp', sql.NVarChar, responseText).input('batchid', sql.NVarChar, batchid)
    //           .query(
    //             "INSERT INTO ecom_and_scan_stages_from_api_backup_Audit (awb_number, xml_response, batchid) VALUES (@trackingNo ,@resp, @batchid)"
    //           );
    //         }
    //         else{
    //           let result = await connection
    //           .request().input('trackingNo', sql.NVarChar, trackingNo).input('resp', sql.NVarChar, responseText).input('batchid', sql.NVarChar, batchid)
    //           .query(
    //             "INSERT INTO ecom_and_scan_stages_from_api_backup_Exception (awb_number, xml_response, batchid) VALUES (@trackingNo ,@resp, @batchid)"
    //           );
    //         }
    //         // var resp =  res.data; 
          
    //         // console.log(result,"Insert Result");
    //         // console.log(responseText,"responseText Result");
    //         if(index%40==0)
    //         {
    //           await sleep(10000);
    //         }
    //   }
    //   // process.exit();

    //   // const file = fs.writeFileSync("ecom" + ".json", "");
    //   //await makeRequest('ecom',eComArray.slice(0,Math.trunc((arr.length/10)*100)));
    //   // // console.log(truncateTable,"Truncate Table Log");
      
    //   // let ten = Math.trunc((eComArray.length / 100) * 10);
    //   // console.log(eComArray.length);

    //   // let twenty = Math.trunc((eComArray.length / 100) * 20);
    //   // let thirty = Math.trunc((eComArray.length / 100) * 30);
    //   // let forty = Math.trunc((eComArray.length / 100) * 40);
    //   // let fifty = Math.trunc((eComArray.length / 100) * 50);
    //   // let sixty = Math.trunc((eComArray.length / 100) * 60);
    //   // let seventy = Math.trunc((eComArray.length / 100) * 70);
    //   // let eighty = Math.trunc((eComArray.length / 100) * 80);
    //   // let ninty = Math.trunc((eComArray.length / 100) * 90);

    //   // let arr1 = eComArray.slice(0, ten);
    //   // let arr2 = eComArray.slice(ten, twenty);
    //   // let arr3 = eComArray.slice(twenty, thirty);
    //   // let arr4 = eComArray.slice(thirty, forty);
    //   // let arr5 = eComArray.slice(forty, fifty);
    //   // let arr6 = eComArray.slice(fifty, sixty);
    //   // let arr7 = eComArray.slice(sixty, seventy);
    //   // let arr8 = eComArray.slice(seventy, eighty);
    //   // let arr9 = eComArray.slice(eighty, ninty);
    //   // let arr10 = eComArray.slice(ninty, eComArray.length);
    //   // // console.log("val");


    //   // let arrayOfPromises = [
    //   //   makeRequest("ecom", arr1),
    //   //   makeRequest("ecom", arr2),
    //   //   makeRequest("ecom", arr3),
    //   //   makeRequest("ecom", arr4),
    //   //   makeRequest("ecom", arr5),
    //   //   makeRequest("ecom", arr6),
    //   //   makeRequest("ecom", arr7),
    //   //   makeRequest("ecom", arr8),
    //   //   makeRequest("ecom", arr9),
    //   //   makeRequest("ecom", arr10)
    //   // ];
    //   // let response = await Promise.all(arrayOfPromises); //.then(res=>{
    //   // // console.log(response, "this is promise resolve");
    //   // // const file = fs.writeFileSync("ecom1.xhtml",
    //   // //   response[1]);
    //   // // });
    //   // // console.log(response,"ecom response");


    //   // let savexmls = await saveXML(response, "ecom");
    //   // console.log(savexmls, "ecom done");


    //   //    require("fs").writeFile(
    //   //       "ecom.xml",
    //   //       response,
    //   //       function (err) { console.log(err ? 'Error :'+err : 'ok') }
    //   //  );

    //   // if (delhiveryArray.length > 0) {
    //   //   console.log("inside delhivery");

    //   //   const file = fs.writeFileSync("delhivery" + ".json", "");
    //   //   //await makeRequest('ecom',eComArray.slice(0,Math.trunc((arr.length/10)*100)));

    //   //   let ten = Math.trunc((delhiveryArray.length / 100) * 10);
    //   //   console.log(delhiveryArray.length);

    //   //   let twenty = Math.trunc((delhiveryArray.length / 100) * 20);
    //   //   let thirty = Math.trunc((delhiveryArray.length / 100) * 30);
    //   //   let forty = Math.trunc((delhiveryArray.length / 100) * 40);
    //   //   let fifty = Math.trunc((delhiveryArray.length / 100) * 50);
    //   //   let sixty = Math.trunc((delhiveryArray.length / 100) * 60);
    //   //   let seventy = Math.trunc((delhiveryArray.length / 100) * 70);
    //   //   let eighty = Math.trunc((delhiveryArray.length / 100) * 80);
    //   //   let ninty = Math.trunc((delhiveryArray.length / 100) * 90);

    //   //   let arr1 = delhiveryArray.slice(0, ten);
    //   //   let arr2 = delhiveryArray.slice(ten, twenty);
    //   //   let arr3 = delhiveryArray.slice(twenty, thirty);
    //   //   let arr4 = delhiveryArray.slice(thirty, forty);
    //   //   let arr5 = delhiveryArray.slice(forty, fifty);
    //   //   let arr6 = delhiveryArray.slice(fifty, sixty);
    //   //   let arr7 = delhiveryArray.slice(sixty, seventy);
    //   //   let arr8 = delhiveryArray.slice(seventy, eighty);
    //   //   let arr9 = delhiveryArray.slice(eighty, ninty);
    //   //   let arr10 = delhiveryArray.slice(ninty, eComArray.length);
    //   //   console.log("val");

    //   //   let arrayOfPromises = [
    //   //     makeRequest("delhivery", arr1),
    //   //     makeRequest("delhivery", arr2),
    //   //     makeRequest("delhivery", arr3),
    //   //     makeRequest("delhivery", arr4),
    //   //     makeRequest("delhivery", arr5),
    //   //     makeRequest("delhivery", arr6),
    //   //     makeRequest("delhivery", arr7),
    //   //     makeRequest("delhivery", arr8),
    //   //     makeRequest("delhivery", arr9),
    //   //     makeRequest("delhivery", arr10)
    //   //   ];
    //   //   let response = await Promise.all(arrayOfPromises); //.then(res=>{
    //   //   console.log(response, "this is promise resolve");

    //   //   for (let i = 0; i < delhiveryArray.length; i++) {
    //   //     const delhiveryElement = delhiveryArray[i];
    //   //     let trackingNo = delhiveryElement.Tracking_Number;
    //   //   //  console.log(Number.parseFloat(trackingNo).toPrecision());
    //   //     trackingNo = Number.parseFloat(trackingNo).toPrecision();
    //   //     //console.log(delhiveryElement);
    //   //     var url = "https://track.delhivery.com/api/packages/json/?token=713c3dba49be9d786774b45235e1f17bf9973a0e&waybill="+trackingNo;
    //   //    // console.log(url);

    //   //       try {
    //   //         var response1 = await axios.get(url);
    //   //         var data = response.data;
    //   //         finalDelhiveryArray.push(data)
    //   //         if(delhiveryElement.Reverse_Tracking_Number){
    //   //           let trackingNo = delhiveryElement.Tracking_Number;
    //   //          // console.log(Number.parseFloat(trackingNo).toPrecision(),"reverse");
    //   //           var url = "https://track.delhivery.com/api/packages/json/?token=713c3dba49be9d786774b45235e1f17bf9973a0e&waybill="+trackingNo;
    //   //           var response2 = await axios.get(url);
    //   //           var data = response.data;
    //   //           finalDelhiveryArray.push(data)

    //   //         }
    //   //         // console.log(data,"delhivery response");
    //   //       } catch (error) {
    //   //         console.log(error);
    //   //       }

    //   //   }

    //   //   //   console.log(JSON.stringify(finalDelhiveryArray,'finaldel'));

    //   //   // }

    //   //   await saveJson(JSON.stringify(response), "delhivery");

    //   //   //await saveJson(JSON.stringify(finalEcomArray,'ecom'))
    //   //   blobName = "CloudStation/Delhivery/delhivery.json";
    //   //   stream = fs.createReadStream("./delhivery.json");
    //   //   stats = fs.statSync("./delhivery.json");
    //   //   var fileSizeInBytes = stats["size"];
    //   //   console.log(fileSizeInBytes);

    //   //   //  calling the upload method for azure blob
    //   //   let azureres = await azure.fileBlob(
    //   //     blobName,
    //   //     stream,
    //   //     fileSizeInBytes,
    //   //     _res => {
    //   //       console.log(_res);
    //   //     }
    //   //   );
    //   // }
    // }
    //ecomReverse++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // if (ecomReverseArray.length > 0) {
    //   console.log("inside ecomReverseArray");

    //   // let truncateTable = await connection.query("Truncate table ecom_and_scan_stages_from_api");

    //   for(let index=0;index<ecomReverseArray.length;index++)
    //   {
    //     // console.log(eComArray[index]);
    //     element = ecomReverseArray[index];
    //     let trackingNo = element.Reverse_Tracking_Number;
    //     // console.log(Number.parseFloat(trackingNo).toPrecision());
    //     trackingNo = Number.parseFloat(trackingNo).toPrecision();
    //     //console.log(delhiveryElement);
        
    //       var url =
    //         "https://plapi.ecomexpress.in/track_me/api/mawbd/?password=WJu47WgNg5mBR35K&username=tcnsclothing112200_women&awb=" +
    //         trackingNo;
    //         let response = await fetch(url);
    //         const responseText = await response.text();
    //         // console.log(typeof json);
            
    //         // var resp =  res.data; 
    //         let result = await connection
    //         .request().input('trackingNo', sql.NVarChar, trackingNo).input('resp', sql.NVarChar, responseText).input('batchid', sql.NVarChar, batchid)
    //         .query(
    //           "INSERT INTO ecom_and_scan_stages_from_api (awb_number, xml_response, batchid) VALUES (@trackingNo ,@resp, @batchid)"
    //         );
    //         // console.log(result,"Insert Result");
    //         // console.log(responseText,"responseText Result");
    //         if(index%40==0)
    //         {
    //           await sleep(10000);
    //         }
    //   }
    //   // process.exit();

    //   // const file = fs.writeFileSync("ecom" + ".json", "");
    //   //await makeRequest('ecom',eComArray.slice(0,Math.trunc((arr.length/10)*100)));
    //   // // console.log(truncateTable,"Truncate Table Log");
      
    //   // let ten = Math.trunc((eComArray.length / 100) * 10);
    //   // console.log(eComArray.length);

    //   // let twenty = Math.trunc((eComArray.length / 100) * 20);
    //   // let thirty = Math.trunc((eComArray.length / 100) * 30);
    //   // let forty = Math.trunc((eComArray.length / 100) * 40);
    //   // let fifty = Math.trunc((eComArray.length / 100) * 50);
    //   // let sixty = Math.trunc((eComArray.length / 100) * 60);
    //   // let seventy = Math.trunc((eComArray.length / 100) * 70);
    //   // let eighty = Math.trunc((eComArray.length / 100) * 80);
    //   // let ninty = Math.trunc((eComArray.length / 100) * 90);

    //   // let arr1 = eComArray.slice(0, ten);
    //   // let arr2 = eComArray.slice(ten, twenty);
    //   // let arr3 = eComArray.slice(twenty, thirty);
    //   // let arr4 = eComArray.slice(thirty, forty);
    //   // let arr5 = eComArray.slice(forty, fifty);
    //   // let arr6 = eComArray.slice(fifty, sixty);
    //   // let arr7 = eComArray.slice(sixty, seventy);
    //   // let arr8 = eComArray.slice(seventy, eighty);
    //   // let arr9 = eComArray.slice(eighty, ninty);
    //   // let arr10 = eComArray.slice(ninty, eComArray.length);
    //   // // console.log("val");


    //   // let arrayOfPromises = [
    //   //   makeRequest("ecom", arr1),
    //   //   makeRequest("ecom", arr2),
    //   //   makeRequest("ecom", arr3),
    //   //   makeRequest("ecom", arr4),
    //   //   makeRequest("ecom", arr5),
    //   //   makeRequest("ecom", arr6),
    //   //   makeRequest("ecom", arr7),
    //   //   makeRequest("ecom", arr8),
    //   //   makeRequest("ecom", arr9),
    //   //   makeRequest("ecom", arr10)
    //   // ];
    //   // let response = await Promise.all(arrayOfPromises); //.then(res=>{
    //   // // console.log(response, "this is promise resolve");
    //   // // const file = fs.writeFileSync("ecom1.xhtml",
    //   // //   response[1]);
    //   // // });
    //   // // console.log(response,"ecom response");


    //   // let savexmls = await saveXML(response, "ecom");
    //   // console.log(savexmls, "ecom done");


    //   //    require("fs").writeFile(
    //   //       "ecom.xml",
    //   //       response,
    //   //       function (err) { console.log(err ? 'Error :'+err : 'ok') }
    //   //  );

    //   // if (delhiveryArray.length > 0) {
    //   //   console.log("inside delhivery");

    //   //   const file = fs.writeFileSync("delhivery" + ".json", "");
    //   //   //await makeRequest('ecom',eComArray.slice(0,Math.trunc((arr.length/10)*100)));

    //   //   let ten = Math.trunc((delhiveryArray.length / 100) * 10);
    //   //   console.log(delhiveryArray.length);

    //   //   let twenty = Math.trunc((delhiveryArray.length / 100) * 20);
    //   //   let thirty = Math.trunc((delhiveryArray.length / 100) * 30);
    //   //   let forty = Math.trunc((delhiveryArray.length / 100) * 40);
    //   //   let fifty = Math.trunc((delhiveryArray.length / 100) * 50);
    //   //   let sixty = Math.trunc((delhiveryArray.length / 100) * 60);
    //   //   let seventy = Math.trunc((delhiveryArray.length / 100) * 70);
    //   //   let eighty = Math.trunc((delhiveryArray.length / 100) * 80);
    //   //   let ninty = Math.trunc((delhiveryArray.length / 100) * 90);

    //   //   let arr1 = delhiveryArray.slice(0, ten);
    //   //   let arr2 = delhiveryArray.slice(ten, twenty);
    //   //   let arr3 = delhiveryArray.slice(twenty, thirty);
    //   //   let arr4 = delhiveryArray.slice(thirty, forty);
    //   //   let arr5 = delhiveryArray.slice(forty, fifty);
    //   //   let arr6 = delhiveryArray.slice(fifty, sixty);
    //   //   let arr7 = delhiveryArray.slice(sixty, seventy);
    //   //   let arr8 = delhiveryArray.slice(seventy, eighty);
    //   //   let arr9 = delhiveryArray.slice(eighty, ninty);
    //   //   let arr10 = delhiveryArray.slice(ninty, eComArray.length);
    //   //   console.log("val");

    //   //   let arrayOfPromises = [
    //   //     makeRequest("delhivery", arr1),
    //   //     makeRequest("delhivery", arr2),
    //   //     makeRequest("delhivery", arr3),
    //   //     makeRequest("delhivery", arr4),
    //   //     makeRequest("delhivery", arr5),
    //   //     makeRequest("delhivery", arr6),
    //   //     makeRequest("delhivery", arr7),
    //   //     makeRequest("delhivery", arr8),
    //   //     makeRequest("delhivery", arr9),
    //   //     makeRequest("delhivery", arr10)
    //   //   ];
    //   //   let response = await Promise.all(arrayOfPromises); //.then(res=>{
    //   //   console.log(response, "this is promise resolve");

    //   //   for (let i = 0; i < delhiveryArray.length; i++) {
    //   //     const delhiveryElement = delhiveryArray[i];
    //   //     let trackingNo = delhiveryElement.Tracking_Number;
    //   //   //  console.log(Number.parseFloat(trackingNo).toPrecision());
    //   //     trackingNo = Number.parseFloat(trackingNo).toPrecision();
    //   //     //console.log(delhiveryElement);
    //   //     var url = "https://track.delhivery.com/api/packages/json/?token=713c3dba49be9d786774b45235e1f17bf9973a0e&waybill="+trackingNo;
    //   //    // console.log(url);

    //   //       try {
    //   //         var response1 = await axios.get(url);
    //   //         var data = response.data;
    //   //         finalDelhiveryArray.push(data)
    //   //         if(delhiveryElement.Reverse_Tracking_Number){
    //   //           let trackingNo = delhiveryElement.Tracking_Number;
    //   //          // console.log(Number.parseFloat(trackingNo).toPrecision(),"reverse");
    //   //           var url = "https://track.delhivery.com/api/packages/json/?token=713c3dba49be9d786774b45235e1f17bf9973a0e&waybill="+trackingNo;
    //   //           var response2 = await axios.get(url);
    //   //           var data = response.data;
    //   //           finalDelhiveryArray.push(data)

    //   //         }
    //   //         // console.log(data,"delhivery response");
    //   //       } catch (error) {
    //   //         console.log(error);
    //   //       }

    //   //   }

    //   //   //   console.log(JSON.stringify(finalDelhiveryArray,'finaldel'));

    //   //   // }

    //   //   await saveJson(JSON.stringify(response), "delhivery");

    //   //   //await saveJson(JSON.stringify(finalEcomArray,'ecom'))
    //   //   blobName = "CloudStation/Delhivery/delhivery.json";
    //   //   stream = fs.createReadStream("./delhivery.json");
    //   //   stats = fs.statSync("./delhivery.json");
    //   //   var fileSizeInBytes = stats["size"];
    //   //   console.log(fileSizeInBytes);

    //   //   //  calling the upload method for azure blob
    //   //   let azureres = await azure.fileBlob(
    //   //     blobName,
    //   //     stream,
    //   //     fileSizeInBytes,
    //   //     _res => {
    //   //       console.log(_res);
    //   //     }
    //   //   );
    //   // }
    // }
    // console.log("ecomDone");

   //elhivery+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // if (delhiveryArray.length > 0) {
    //   console.log("inside delhivery");

    //   const file = fs.writeFileSync("delhivery" + ".json", "");
    //   //await makeRequest('ecom',eComArray.slice(0,Math.trunc((arr.length/10)*100)));

    //   // let ten = Math.trunc((delhiveryArray.length / 100) * 10);
    //   // console.log(delhiveryArray.length);

    //   // let twenty = Math.trunc((delhiveryArray.length / 100) * 20);
    //   // let thirty = Math.trunc((delhiveryArray.length / 100) * 30);
    //   // let forty = Math.trunc((delhiveryArray.length / 100) * 40);
    //   // let fifty = Math.trunc((delhiveryArray.length / 100) * 50);
    //   // let sixty = Math.trunc((delhiveryArray.length / 100) * 60);
    //   // let seventy = Math.trunc((delhiveryArray.length / 100) * 70);
    //   // let eighty = Math.trunc((delhiveryArray.length / 100) * 80);
    //   // let ninty = Math.trunc((delhiveryArray.length / 100) * 90);

    //   // let arr1 = delhiveryArray.slice(0, ten);
    //   // let arr2 = delhiveryArray.slice(ten, twenty);
    //   // let arr3 = delhiveryArray.slice(twenty, thirty);
    //   // let arr4 = delhiveryArray.slice(thirty, forty);
    //   // let arr5 = delhiveryArray.slice(forty, fifty);
    //   // let arr6 = delhiveryArray.slice(fifty, sixty);
    //   // let arr7 = delhiveryArray.slice(sixty, seventy);
    //   // let arr8 = delhiveryArray.slice(seventy, eighty);
    //   // let arr9 = delhiveryArray.slice(eighty, ninty);
    //   // let arr10 = delhiveryArray.slice(ninty, eComArray.length);
    //   // console.log("val");

    //   // let arrayOfPromises = [
    //   //   makeRequest("delhivery", arr1),
    //   //   makeRequest("delhivery", arr2),
    //     // makeRequest("delhivery", arr3),
    //   //   makeRequest("delhivery", arr4),
    //   //   makeRequest("delhivery", arr5),
    //   //   makeRequest("delhivery", arr6),
    //   //   makeRequest("delhivery", arr7),
    //   //   makeRequest("delhivery", arr8),
    //   //   makeRequest("delhivery", arr9),
    //   //   makeRequest("delhivery", arr10)
    //   // ];
    //   // let response = await Promise.all(arrayOfPromises); //.then(res=>{
    //   // console.log(response, "this is promise resolve");

    //   for (let i = 0; i < delhiveryArray.length; i++) {
    //     const delhiveryElement = delhiveryArray[i];
    //     let trackingNo = delhiveryElement.Tracking_Number;
    //     //  console.log(Number.parseFloat(trackingNo).toPrecision());
    //     trackingNo = Number.parseFloat(trackingNo).toPrecision();
    //     // console.log(trackingNo);
    //     var url = "https://track.delhivery.com/api/packages/json/?token=713c3dba49be9d786774b45235e1f17bf9973a0e&waybill=" + trackingNo;
    //     // console.log(url);

    //     try {
    //       console.log(i);
          
    //       // var response1 = await axios.get(url);
    //       let res = await fetch(url);
    //       let responseJSON = await res.json();
    //        console.log(responseJSON);
    //       if(i%20==0)
    //       {
    //         await sleep(10000);
    //       }
    //       if(responseJSON.Error!='No such waybill or Order Id found')
    //       {
    //         // console.log("TRUEEE");
    //       finalDelhiveryArray.push(responseJSON)
            
    //       }
        
    //       // if (delhiveryElement.Reverse_Tracking_Number) {
    //       //   let trackingNo = delhiveryElement.Reverse_Tracking_Number;
    //       //   // console.log(Number.parseFloat(trackingNo).toPrecision(),"reverse");
    //       //   var url = "https://track.delhivery.com/api/packages/json/?token=713c3dba49be9d786774b45235e1f17bf9973a0e&waybill=" + trackingNo;
    //       //   let res1 = await fetch(url);
    //       //   let responseJSON1 = await res1.json();
    //       //   console.log(responseJSON1);
            
    //       //   finalDelhiveryArray.push(responseJSON1)

    //       // }
          
    //     } catch (error) {
    //       console.log(error);
    //       let obj={
    //         functionaName:"Delhivery API Error",
    //         error:error,
    //         stringError:JSON.stringify(error,undefined,4)
    //        }
    //        mailer.main(obj)
    //     }

    //   }
    //   if(delhiveryReverseArray.length>0)
    //   {
    //     for (let i = 0; i < delhiveryReverseArray.length; i++) {
    //       const delhiveryElement = delhiveryReverseArray[i];
    //       let trackingNo = delhiveryElement.Reverse_Tracking_Number;
    //       //  console.log(Number.parseFloat(trackingNo).toPrecision());
    //       trackingNo = Number.parseFloat(trackingNo).toPrecision();
    //       // console.log(trackingNo);
    //       var url = "https://track.delhivery.com/api/packages/json/?token=713c3dba49be9d786774b45235e1f17bf9973a0e&waybill=" + trackingNo;
    //       // console.log(url);
  
    //       try {
    //         console.log(i);
            
    //         let res = await fetch(url);
    //       let responseJSON = await res.json();
    //         // console.log(responseJSON);
    //         if(i%40==0)
    //         {
    //           await sleep(10000);
    //         }
    //         if(responseJSON.Error!='No such waybill or Order Id found')
    //         {
    //           // console.log("TRUEEE");
    //         finalDelhiveryArray.push(responseJSON)
              
    //         }
          
        
    //       } catch (error) {
    //         console.log(error);
    //         let obj={
    //           functionaName:"Delhivery API Error",
    //           error:error,
    //           stringError:JSON.stringify(error,undefined,4)
    //          }
    //          mailer.main(obj)
    //       }
  
    //     }
    //   }
    //   //   console.log(JSON.stringify(finalDelhiveryArray,'finaldel'));

    //   // }
    //   console.log(finalDelhiveryArray.length,"Final Delhivery length");

    //   await saveJson(JSON.stringify(finalDelhiveryArray), "delhivery");

    //   // await saveJson(JSON.stringify(finalEcomArray,'ecom'))
    //   blobName = "CloudStation/Delhivery/delhivery.json";
    //   stream = fs.createReadStream("./delhivery.json");
    //   stats = fs.statSync("./delhivery.json");
    //   var fileSizeInBytes = stats["size"];
    //   console.log(fileSizeInBytes);

    //   //  calling the upload method for azure blob
    //   let azureres = await azure.fileBlob(
    //     blobName,
    //     stream,
    //     fileSizeInBytes,
    //     _res => {
    //       console.log(_res);
    //     }
    //   );
    // }
    //shadowfax+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // if(shadowfaxArray.length>0)
    // {
    //   console.log("inside shadowfax");
    //   const file = fs.writeFileSync("shadowfax" + ".json", "");
    //   //await makeRequest('ecom',eComArray.slice(0,Math.trunc((arr.length/10)*100)));

    //   // 
    //   for (let i = 0; i <shadowfaxArray.length; i++) {
    //     const shadowFaxElement = shadowfaxArray[i];
    //     let trackingNo = shadowFaxElement.Tracking_Number;
    //     //  console.log(Number.parseFloat(trackingNo).toPrecision());
    //     // trackingNo = Number.parseFloat(trackingNo).toPrecision();
    //     // console.log(trackingNo);
    //     var url = "https://saruman.shadowfax.in/api/v1/clients/requests/" +trackingNo;
    //     headers = {
    //      headers: {
    //        'Authorization': `Token e8e3c72ef240f36da5ead3e86f7e671c87146260`
    //      }
    //     }
      
       
       
    //     // console.log(url);

    //     try {
    //       console.log(i);
          
    //       // var response1 = await axios.get(url);
    //       let response2 = await axios.get(url,headers);
    //       // console.log(response2.data);

    //       var data = response2.data
    //       // console.log(responseJSON);
    //       if(data.message=="Success")
    //       {
    //         finalShadowFaxArray.push(data)

    //       }
    //       if(i%20==0)
    //       {
    //         await sleep(10000);
    //       }
          
    //     } catch (error) {
    //       console.log(error);
    //       let obj={
    //         functionaName:"ShadowFax API Error",
    //         error:error,
    //         stringError:JSON.stringify(error,undefined,4)
    //        }
    //       //  mailer.main(obj)
    //     }

    //   }

    //   //   console.log(JSON.stringify(finalDelhiveryArray,'finaldel'));

    //   // }
    //   console.log(finalShadowFaxArray.length,"finalShadowFaxArray length");

    //   await saveJson(JSON.stringify(finalShadowFaxArray), "shadowfax");


      
    //   blobName = "CloudStation/Shadowfax/shadowfax.json";
    //   stream = fs.createReadStream("./shadowfax.json");
    //   stats = fs.statSync("./shadowfax.json");
    //   var fileSizeInBytes = stats["size"];
    //   console.log(fileSizeInBytes);

    //   //  calling the upload method for azure blob
    //   let azureres = await azure.fileBlob(
    //     blobName,
    //     stream,
    //     fileSizeInBytes,
    //     _res => {
    //       console.log(_res);
    //     }
    //   );

    // }
    //reverseShadowfax+++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // if(reverseShadowfaxArray.length>0)
    // {
    //   console.log("inside reverseShadowfax");
    //   const file = fs.writeFileSync("shadowfaxReverse" + ".json", "");
    //   //await makeRequest('ecom',eComArray.slice(0,Math.trunc((arr.length/10)*100)));

    //   // 
    //   for (let i = 0; i <reverseShadowfaxArray.length; i++) {
    //     const shadowFaxElement = reverseShadowfaxArray[i];
    //     let trackingNo = shadowFaxElement.Reverse_Tracking_Number;
    //     //  console.log(Number.parseFloat(trackingNo).toPrecision());
    //     // trackingNo = Number.parseFloat(trackingNo).toPrecision();
    //     // console.log(trackingNo);
    //     var url = "https://reverse.shadowfax.in/api/v3/clients/requests/" +trackingNo;
    //     headers = {
    //      headers: {
    //        'Authorization': `Token e8e3c72ef240f36da5ead3e86f7e671c87146260`
    //      }
    //     }
      
       
       
    //     // console.log(url);

    //     try {
    //       console.log(i);
          
    //       // var response1 = await axios.get(url);
    //       let response2 = await axios.get(url,headers);
    //       // console.log(response2.data);

    //       var data = response2.data
    //       // console.log(responseJSON);
    //       if(data.client_order_number)
    //       {
    //         finalReverseShadowFaxArray.push(data)

    //       }

    //       if(i%20==0)
    //       {
    //         await sleep(10000);
    //       }
        
            
        
    //       // if (delhiveryElement.Reverse_Tracking_Number) {
    //       //   let trackingNo = delhiveryElement.Reverse_Tracking_Number;
    //       //   // console.log(Number.parseFloat(trackingNo).toPrecision(),"reverse");
    //       //   var url = "https://track.delhivery.com/api/packages/json/?token=713c3dba49be9d786774b45235e1f17bf9973a0e&waybill=" + trackingNo;
    //       //   var response2 = await axios.get(url);
    //       //   var data = response.data;
    //       //   finalDelhiveryArray.push(data)

    //       // }
    //       // console.log(data,"delhivery response");
          
    //     } catch (error) {
    //       console.log(error);
    //       let obj={
    //         functionaName:"ShadowFax API Error",
    //         error:error,
    //         stringError:JSON.stringify(error,undefined,4)
    //        }
    //       //  mailer.main(obj)
    //     }

    //   }

    //   //   console.log(JSON.stringify(finalDelhiveryArray,'finaldel'));

    //   // }
    //   console.log(finalReverseShadowFaxArray.length,"finalReverseShadowFaxArray length");

    //   await saveJson(JSON.stringify(finalReverseShadowFaxArray), "shadowfaxReverse");


      
    //   blobName = "CloudStation/Shadowfax/shadowfaxReverse.json";
    //   stream = fs.createReadStream("./shadowfaxReverse.json");
    //   stats = fs.statSync("./shadowfaxReverse.json");
    //   var fileSizeInBytes = stats["size"];
    //   console.log(fileSizeInBytes);

    //   //  calling the upload method for azure blob
    //   let azureres = await azure.fileBlob(
    //     blobName,
    //     stream,
    //     fileSizeInBytes,
    //     _res => {
    //       console.log(_res);
    //     }
    //   );

    // }
    //===================================================================
  } catch (error) {
    let obj={
      functionaName:"Ecomm DB Connection Error",
      error:error,
      stringError:JSON.stringify(error,undefined,4)
     }
    //  mailer.main(obj);
    console.log(error);
    
  }
  },


  googleAnalytics: async () => {
    const service_account = require("./jsonkey.json");
    console.log(service_account.client_email);

    const reporting = google.analyticsreporting("v4");

    let scopes = ["https://www.googleapis.com/auth/analytics"];

    let jwt = new google.auth.JWT(
      service_account.client_email,
      null,
      service_account.private_key,
      scopes
    );

    let getReports = async function (reports) {
      await jwt.authorize();

      let request = {
        headers: { "Content-Type": "application/json" },
        auth: jwt,
        resource: reports
      };

      return await reporting.reports.batchGet(request);
    };
    let dt = new Date();
    dt.setDate(dt.getUTCDate() - 1);
    dt = dt.toISOString().split('T')[0]

    let view1W = {
      reportRequests: [
        {
          viewId: "93359683",
          dateRanges: [
            {
              startDate: dt,
              endDate: dt
            }
          ],
          metrics: [
            {
              expression: "ga:users"
            },
            {
              expression: "ga:sessions"
            },

            {
              expression: "ga:bounceRate"
            },
            {
              expression: "ga:pageviews"
            },
            {
              expression: "ga:productAddsToCart"
            },
            {
              expression: "ga:productCheckouts"
            },

            {
              expression: "ga:itemQuantity"
            },

            {
              expression: "ga:productDetailViews"
            },
            {
              expression: "ga:productListClicks"
            },
            {
              expression: "ga:productListViews"
            }
          ],
          dimensions: [
            {
              name: "ga:productSku"
            },
            {
              name: "ga:date"
            }
          ],
          pageSize: 5000
        }
      ]
    };
    let view1Aur = {
      reportRequests: [
        {
          viewId: "149296247",
          dateRanges: [
            {
              startDate: dt,
              endDate: dt
            }
          ],
          metrics: [
            {
              expression: "ga:users"
            },
            {
              expression: "ga:sessions"
            },

            {
              expression: "ga:bounceRate"
            },
            {
              expression: "ga:pageviews"
            },
            {
              expression: "ga:productAddsToCart"
            },
            {
              expression: "ga:productCheckouts"
            },

            {
              expression: "ga:itemQuantity"
            },

            {
              expression: "ga:productDetailViews"
            },
            {
              expression: "ga:productListClicks"
            },
            {
              expression: "ga:productListViews"
            }
          ],
          dimensions: [
            {
              name: "ga:productSku"
            },
            {
              name: "ga:date"
            }
          ],
          pageSize: 5000
        }
      ]
    };
    let view2W = {
      reportRequests: [
        {
          viewId: "93359683",
          dateRanges: [
            {
              startDate: dt,
              endDate: dt
            }
          ],
          metrics: [
            {
              expression: "ga:users"
            },
            {
              expression: "ga:sessions"
            },
            {
              expression: "ga:pageviews"
            },
            {
              expression: "ga:bounceRate"
            },

            {
              expression: "ga:newUsers"
            },
            {
              expression: "ga:bounces"
            }
          ],
          dimensions: [
            {
              name: "ga:date"
            }
          ]
        }
      ]
    };
    let view2Aur = {
      reportRequests: [
        {
          viewId: "149296247",
          dateRanges: [
            {
              startDate: dt,
              endDate: dt
            }
          ],
          metrics: [
            {
              expression: "ga:users"
            },
            {
              expression: "ga:sessions"
            },
            {
              expression: "ga:pageviews"
            },
            {
              expression: "ga:bounceRate"
            },

            {
              expression: "ga:newUsers"
            },
            {
              expression: "ga:bounces"
            }
          ],
          dimensions: [
            {
              name: "ga:date"
            }
          ]
        }
      ]
    };
    let view3W = {
      reportRequests: [
        {
          viewId: "93359683",
          dateRanges: [
            {
              startDate: dt,
              endDate: dt
            }
          ],
          metrics: [
            {
              expression: "ga:users"
            },
            {
              expression: "ga:sessions"
            },
            {
              expression: "ga:pageviews"
            },
            {
              expression: "ga:bounceRate"
            },

            {
              expression: "ga:newUsers"
            },

            {
              expression: "ga:productAddsToCart"
            },
            {
              expression: "ga:productCheckouts"
            },
            {
              expression: "ga:uniquePurchases"
            },
            {
              expression: "ga:productListViews"
            }
          ],
          dimensions: [
            {
              name: "ga:sourceMedium"
            },
            {
              name: "ga:channelGrouping"
            },
            {
              name: "ga:date"
            }
          ]
        }
      ]
    };
    let view3Aur = {
      reportRequests: [
        {
          viewId: "149296247",
          dateRanges: [
            {
              startDate: dt,
              endDate: dt
            }
          ],
          metrics: [
            {
              expression: "ga:users"
            },
            {
              expression: "ga:sessions"
            },
            {
              expression: "ga:pageviews"
            },
            {
              expression: "ga:bounceRate"
            },

            {
              expression: "ga:newUsers"
            },

            {
              expression: "ga:productAddsToCart"
            },
            {
              expression: "ga:productCheckouts"
            },
            {
              expression: "ga:uniquePurchases"
            },
            {
              expression: "ga:productListViews"
            }
          ],
          dimensions: [
            {
              name: "ga:sourceMedium"
            },
            {
              name: "ga:channelGrouping"
            },

            {
              name: "ga:date"
            }
          ]
        }
      ]
    };
    getReports(view1Aur)
      .then(async response => {
        console.log(response.data);
        fs.writeFileSync("gaview1aur" + ".json", JSON.stringify(response.data));
        blobName = "CloudStation/googleAnalytics/gaview1aur.json";
        stream = fs.createReadStream("./gaview1aur.json");
        stats = fs.statSync("./gaview1aur.json");
        var fileSizeInBytes = stats["size"];
        console.log(fileSizeInBytes);
        await azure.fileBlob(blobName, stream, fileSizeInBytes, _res => {
          console.log(_res);
        });
      })
      .catch(e =>{
         let obj={
          functionaName:"GA View 1 Aureila Get Reports",
          error:e,
          stringError:JSON.stringify(e,undefined,4)
         }
         mailer.main(obj)
         console.log(e,"error")
         console.log(JSON.stringify(e),"stringyf");
         

        });




    getReports(view1W)
      .then(async response => {
        console.log(response.data);
        fs.writeFileSync("gaview1w" + ".json", JSON.stringify(response.data));
        blobName = "CloudStation/googleAnalytics/gaview1w.json";
        stream = fs.createReadStream("./gaview1w.json");
        stats = fs.statSync("./gaview1w.json");
        var fileSizeInBytes = stats["size"];
        console.log(fileSizeInBytes);
        await azure.fileBlob(blobName, stream, fileSizeInBytes, _res => {
          console.log(_res);
        });
      })
      .catch(e =>{
        let obj={
         functionaName:"GA View 1 W Get Reports",
         error:e,
         stringError:JSON.stringify(e,undefined,4)
        }
        mailer.main(obj)
        console.log(e,"error")
        console.log(JSON.stringify(e),"stringyf");
        

       });

    getReports(view2Aur)
      .then(async response => {
        console.log(response.data);
        fs.writeFileSync("gaview2aur" + ".json", JSON.stringify(response.data));
        blobName = "CloudStation/googleAnalytics/gaview2aur.json";
        stream = fs.createReadStream("./gaview2aur.json");
        stats = fs.statSync("./gaview2aur.json");
        var fileSizeInBytes = stats["size"];
        console.log(fileSizeInBytes);
        await azure.fileBlob(blobName, stream, fileSizeInBytes, _res => {
          console.log(_res);
        });
      })
      .catch(e =>{
        let obj={
         functionaName:"GA View 2 Aureila Get Reports",
         error:e,
         stringError:JSON.stringify(e,undefined,4)
        }
        mailer.main(obj)
        console.log(e,"error")
        console.log(JSON.stringify(e),"stringyf");
        

       });

    getReports(view2W)
      .then(async response => {
        console.log(response.data);
        fs.writeFileSync("gaview2w" + ".json", JSON.stringify(response.data));
        blobName = "CloudStation/googleAnalytics/gaview2w.json";
        stream = fs.createReadStream("./gaview2w.json");
        stats = fs.statSync("./gaview2w.json");
        var fileSizeInBytes = stats["size"];
        console.log(fileSizeInBytes);
        await azure.fileBlob(blobName, stream, fileSizeInBytes, _res => {
          console.log(_res);
        });
      })
      .catch(e =>{
        let obj={
         functionaName:"GA View 2 W Get Reports",
         error:e,
         stringError:JSON.stringify(e,undefined,4)
        }
        mailer.main(obj)
        console.log(e,"error")
        console.log(JSON.stringify(e),"stringyf");
        

       });

    getReports(view3Aur)
      .then(async response => {
        console.log(response.data);
        fs.writeFileSync("gaview3aur" + ".json", JSON.stringify(response.data));
        blobName = "CloudStation/googleAnalytics/gaview3aur.json";
        stream = fs.createReadStream("./gaview3aur.json");
        stats = fs.statSync("./gaview3aur.json");
        var fileSizeInBytes = stats["size"];
        console.log(fileSizeInBytes);
        await azure.fileBlob(blobName, stream, fileSizeInBytes, _res => {
          console.log(_res);
        });
      })
      .catch(e =>{
        let obj={
          functionaName:"GA View 3 Aureila Get Reports",
          error:e,
          stringError:JSON.stringify(e,undefined,4)
         }
         mailer.main(obj)
         console.log(e,"error")
         console.log(JSON.stringify(e),"stringyf");
         
 
       });

    getReports(view3W)
      .then(async response => {
        console.log(response.data);
        fs.writeFileSync("gaview3w" + ".json", JSON.stringify(response.data));
        blobName = "CloudStation/googleAnalytics/gaview3w.json";
        stream = fs.createReadStream("./gaview3w.json");
        stats = fs.statSync("./gaview3w.json");
        var fileSizeInBytes = stats["size"];
        console.log(fileSizeInBytes);
        await azure.fileBlob(blobName, stream, fileSizeInBytes, _res => {
          console.log(_res);
        });
      })
      .catch(e =>{
        let obj={
         functionaName:"GA View 3 W Get Reports",
         error:e,
         stringError:JSON.stringify(e,undefined,4)
        }
        mailer.main(obj)
        console.log(e,"error")
        console.log(JSON.stringify(e),"stringyf");
        

       });





  },

  freshDesk: async () => {
    let res;
    let condition = true;
    const file = fs.writeFileSync("freshdesk" + ".json", "");
    let freshDesk = [];
    for (let index = 1; index <= 300; index++) {
      try {
        let url =
          // https://tcnsclothing.freshdesk.com/api/v2/tickets?per_page=100&updated_since=2019-09-01T02:00:00Z&page=300
          "https://tcnsclothing.freshdesk.com/api/v2/tickets?page=" +
          index +
          "&per_page=100&updated_since=2019-10-16T02:00:00Z";
        res = await axios.get(url, {
          auth: {
            username: "tcnsclothing.ads@gmail.com",
            password: "wforw@123"
          }
        });
      } catch (error) {
        console.log(error, "error in page api");
        let obj={
          functionaName:"Freshdesk Get Tickets API Error",
          error:error,
          stringError:JSON.stringify(error,undefined,4)
         }
         mailer.main(obj)
         console.log(error,"error")
         console.log(JSON.stringify(error),"stringyf");
         
      }
      for (let i = 0; i < res.data.length; i++) {
        const element = res.data[i];
        var today = new Date();

        // console.log(today, "today");

        var createdAt = new Date(element.created_at);

        var datediff = Math.floor(
          (Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()) -
            Date.UTC(
              createdAt.getFullYear(),
              createdAt.getMonth(),
              createdAt.getDate()
            )) /
          (1000 * 60 * 60 * 24)
        );
        // console.log(element.created_at, "element date");

        // console.log(datediff);
        if (datediff > 8) {

          console.log("breaking");
          // continue;
          condition = false;
          break;

        }
        // else if(datediff > 8){

        //   condition = false;
        //   break;
        // }

        else {
          try {
            let ticketUrl =
              "https://tcnsclothing.freshdesk.com/helpdesk/tickets/" +
              element.id +
              ".json";
            let response = await axios.get(ticketUrl, {
              auth: {
                username: "tcnsclothing.ads@gmail.com",
                password: "wforw@123"
              }
            });

            freshDesk.push(response.data);
          } catch (error) {
            console.log(error, "error in tick api");
    
            let obj={
              functionaName:"Freshdesk Get Individual Tickets API Error",
              error:error,
              stringError:JSON.stringify(error,undefined,4)
             }
             mailer.main(obj)
             console.log(error,"error")
             console.log(JSON.stringify(error),"stringyf");
          }
        }
      }
      if (!condition) {
        console.log("breaking outer");

        break;
      }
    }
    //var output  = h2p(JSON.stringify(freshDesk))

    await saveJson(JSON.stringify(freshDesk), "freshdesk");
    blobName = "CloudStation/Freshdesk/freshdesk.json";
    stream = fs.createReadStream("./freshdesk.json");
    stats = fs.statSync("./freshdesk.json");
    var fileSizeInBytes = stats["size"];
    console.log(fileSizeInBytes);

    //  calling the upload method for azure blob
    let azureres = await azure.fileBlob(
      blobName,
      stream,
      fileSizeInBytes,
      _res => {
        console.log(_res);
      }
    );
  }
};






// await sql
//   .connect(_config.dbConfig)
//   .then(pool => {
//     // Query
//     console.log('connected');

//      let b = await pool
//       .request()
//       .query("select * from pos_data");
//   })
//   .then(result => {
//     console.log(result.recordset);
//   })
//   .catch(err => {
//     // ... error checks
//     console.log(err)
//   });
// }



async function getJobBody(type) {
  console.log("Job Started ...");
  while (true) {
    // print(type,"TYPE");
    let status = await jobStatus(type);
    //console.log(status[0]);

    //checking if the job has been completed
    if (status[0] == "true") {
      if(type=='saleorder30')
      {
        parseString(status[1], function (err, result) {
          filePath30 =
            result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
              "GetExportJobStatusResponse"
            ][0].FilePath[0];
        });
      }
      else{
        parseString(status[1], function (err, result) {
          filePath =
            result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
              "GetExportJobStatusResponse"
            ][0].FilePath[0];
        });
      }
    

      //   saving the csv file received
      if(type=='saleorder30')
      {
      let request30 = await save30(filePath30,type);
      }
      else{
      let request = await save(filePath,type);

      }
      console.log("Job finished...");

      //creating the stream to pass in blob service
      timestamp = new Date();
      timestamp = timestamp.getTime();
      let stats;
      if (type == "saleorder") {
        console.log("in saleorder");

        blobName = "CloudStation/UniComSale/" + type + ".csv";
        stream = fs.createReadStream("./saleorder.csv");
        // stream = fs.createReadStream("./file.csv");
        //stats = fs.statSync("./file.csv");
        stats = fs.statSync("./saleorder.csv");
      } 
      else if (type == "saleorder1") {
        console.log("in saleorder1");

        blobName = "CloudStation/UniComSale/" + type + ".csv";
        stream = fs.createReadStream("./saleorder1.csv");
        // stream = fs.createReadStream("./file.csv");
        //stats = fs.statSync("./file.csv");
        stats = fs.statSync("./saleorder1.csv");
      } 
      else if (type == "saleorder30") {
        console.log("in saleorder30");

        blobName = "CloudStation/UniComSale/" + type + ".csv";
        stream = fs.createReadStream("./saleorder30.csv");
        //stream = fs.createReadStream("./file.csv");
        //stats = fs.statSync("./file.csv");
        stats = fs.statSync("./saleorder30.csv");

      } 
       else if (type == "reversepickup") {
        blobName = "CloudStation/UniComRev/" + type + ".csv";
        stream = fs.createReadStream("./reversepickup.csv");
        //stream = fs.createReadStream("./file.csv");
        //stats = fs.statSync("./file.csv");
        stats = fs.statSync("./reversepickup.csv");
      }
      //New Changes xmlshipping45dayback
      else if (type == "shippingpackagetimeline_glaucus") {
        blobName = "CloudStation/ship_pack_timeline/" + type + ".csv";
        stream = fs.createReadStream("./shippingpackagetimeline_glaucus.csv");
        stats = fs.statSync("./shippingpackagetimeline_glaucus.csv");
      }
      //new...............
      else if (type == "shippingpackagetimeline_glaucus_1") {
        blobName = "CloudStation/ship_pack_timeline/" + type + ".csv";
        stream = fs.createReadStream("./shippingpackagetimeline_glaucus_1.csv");
        stats = fs.statSync("./shippingpackagetimeline_glaucus_1.csv");
      }
      //......................
      else if (type == "shippingpackagetimeline_TCNS_113") {
        blobName = "CloudStation/ship_pack_timeline/" + type + ".csv";
        stream = fs.createReadStream("./shippingpackagetimeline_TCNS_113.csv");
        stats = fs.statSync("./shippingpackagetimeline_TCNS_113.csv");
      }
      //new....................
      else if (type == "shippingpackagetimeline_TCNS_113_1") {
        blobName = "CloudStation/ship_pack_timeline/" + type + ".csv";
        stream = fs.createReadStream("./shippingpackagetimeline_TCNS_113_1.csv");
        stats = fs.statSync("./shippingpackagetimeline_TCNS_113_1.csv");
      }
      //....................
      else if (type == "shippingpackagetimeline_GR_GL_01") {
        blobName = "CloudStation/ship_pack_timeline/" + type + ".csv";
        stream = fs.createReadStream("./shippingpackagetimeline_GR_GL_01.csv");
        stats = fs.statSync("./shippingpackagetimeline_GR_GL_01.csv");
      }
      //new....................
      else if (type == "shippingpackagetimeline_GR_GL_01_1") {
        blobName = "CloudStation/ship_pack_timeline/" + type + ".csv";
        stream = fs.createReadStream("./shippingpackagetimeline_GR_GL_01_1.csv");
        stats = fs.statSync("./shippingpackagetimeline_GR_GL_01_1.csv");
      }
      //....................
      //End New Changes
      else {
        blobName = "CloudStation/UniComInv/" + type + ".csv";
        stream = fs.createReadStream("./inventorysnapshot.csv");
        //stream = fs.createReadStream("./file.csv");
        stats = fs.statSync("./inventorysnapshot.csv");
        //stats = fs.statSync("./file.csv");
      }

      var fileSizeInBytes = stats["size"];
      console.log(fileSizeInBytes);

      //   calling the upload method for azure blob
      let azureres = await azure.fileBlob(
        blobName,
        stream,
        fileSizeInBytes,
        _res => {
          console.log(_res);
        }
      );

      //removing the file downloaded

      // fs.unlink("./file.csv", (err, res) => {
      //   if (err) {
      //     console.log("error Occured while unlinking");
      //   }
      // });
      
      if(type=="saleorder")
      {
        fs.unlink("./saleorder.csv", (err, res) => {
          if (err) {
            console.log("error Occured while unlinking");
          }
        });
      }
      if(type=="saleorder1")
      {
        fs.unlink("./saleorder1.csv", (err, res) => {
          if (err) {
            console.log("error Occured while unlinking");
          }
        });
      }
      if(type=="saleorder30")
      {
        fs.unlink("./saleorder30.csv", (err, res) => {
          if (err) {
            console.log("error Occured while unlinking");
          }
        });
      }
      if(type=="reversepickup")
      {
        fs.unlink("./reversepickup.csv", (err, res) => {
          if (err) {
            console.log("error Occured while unlinking");
          }
        });
      }
      if(type=="inventorysnapshot")
      {
        fs.unlink("./inventorysnapshot.csv", (err, res) => {
          if (err) {
            console.log("error Occured while unlinking");
          }
        });
      }
      // if(type=="saleorder" ||type=="saleorder30" ||type=="reversepickup" || type=="inventorysnapshot")
      // {
      //   fs.unlink("./file.csv", (err, res) => {
      //     if (err) {
      //       console.log("error Occured while unlinking");
      //     }
      //   });
      // }
      if(type=="shippingpackagetimeline_glaucus")
      {
        fs.unlink("./shippingpackagetimeline_glaucus.csv", (err, res) => {
          if (err) {
            console.log("error Occured while unlinking");
          }
        });
      }
      //new......................
      if(type=="shippingpackagetimeline_glaucus_1")
      {
        fs.unlink("./shippingpackagetimeline_glaucus_1.csv", (err, res) => {
          if (err) {
            console.log("error Occured while unlinking");
          }
        });
      }
      //........................
      if(type=="shippingpackagetimeline_TCNS_113")
      {
        fs.unlink("./shippingpackagetimeline_TCNS_113.csv", (err, res) => {
          if (err) {
            console.log("error Occured while unlinking");
          }
        });
      }
      //new......................
      if(type=="shippingpackagetimeline_TCNS_113_1")
      {
        fs.unlink("./shippingpackagetimeline_TCNS_113_1.csv", (err, res) => {
          if (err) {
            console.log("error Occured while unlinking");
          }
        });
      }
      //.......................
      if(type=="shippingpackagetimeline_GR_GL_01")
      {
        fs.unlink("./shippingpackagetimeline_GR_GL_01.csv", (err, res) => {
          if (err) {
            console.log("error Occured while unlinking");
          }
        });
      }
      //new......................
      if(type=="shippingpackagetimeline_GR_GL_01_1")
      {
        fs.unlink("./shippingpackagetimeline_GR_GL_01_1.csv", (err, res) => {
          if (err) {
            console.log("error Occured while unlinking");
          }
        });
      }
      //.......................
      // file_shipping
      return true;
    }
  }
}

// getting the status of the job
function jobStatus(type) {
  let promise = new Promise(async (resolve, reject) => {
    let url = "https://tcns.unicommerce.com/services/soap/?version=1.9";
    //New Changes
    let dataurl = "https://tcns.unicommerce.com:443/services/soap/?version=1.9&facility=glaucus";
    let dataurlTCNS_113 = "https://tcns.unicommerce.com:443/services/soap/?version=1.9&facility=TCNS_113";
    let dataurlGR_GL_01 = "https://tcns.unicommerce.com:443/services/soap/?version=1.9&facility=GR_GL_01";
    //End New Changes
    let xml = fs.readFileSync("xmlbody/getcsv.xml", "utf-8");
    xml = xml.replace("@jobcode", jobCode);
    let xml30 = fs.readFileSync("xmlbody/getcsv.xml", "utf-8");
    xml30 = xml30.replace("@jobcode", jobCode30);
    //New Changes
    let xmlshipping = fs.readFileSync("xmlbody/getcsv.xml", "utf-8");
    xmlshipping = xmlshipping.replace("@jobcode", jobCode);
    let xmlsaleorder30day = fs.readFileSync("xmlbody/getcsv.xml", "utf-8");
    xmlsaleorder30day = xmlsaleorder30day.replace("@jobcode", jobCode);
    let xmlsaleorder30dayback = fs.readFileSync("xmlbody/getcsv.xml", "utf-8");
    xmlsaleorder30dayback = xmlsaleorder30dayback.replace("@jobcode", jobCode);
    let xmlshipping45dayback = fs.readFileSync("xmlbody/getcsv.xml", "utf-8");
    xmlshipping45dayback = xmlshipping45dayback.replace("@jobcode", jobCode);
    //End New Changes 

    // console.log("inside");
    // console.log(type);
    
    if(type=='saleorder')
    {
      let { response } = await soapRequest({
        url: url,
        headers: sampleHeaders,
        xml: xmlsaleorder30day
      });
      let { headers, body, statusCode } = response;
      parseString(body, function (err, result) {
        let status =
          result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
            "GetExportJobStatusResponse"
          ][0].Successful[0];
        resolve([status, body]);
      });
    }
    if(type=='saleorder1')
    {
      let { response } = await soapRequest({
        url: url,
        headers: sampleHeaders,
        xml: xmlsaleorder30dayback
      });
      let { headers, body, statusCode } = response;
      parseString(body, function (err, result) {
        let status =
          result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
            "GetExportJobStatusResponse"
          ][0].Successful[0];
        resolve([status, body]);
      });
    }
    if(type=='reversepickup')
    {
      let { response } = await soapRequest({
        url: url,
        headers: sampleHeaders,
        xml: xml
      });
      let { headers, body, statusCode } = response;
      parseString(body, function (err, result) {
        let status =
          result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
            "GetExportJobStatusResponse"
          ][0].Successful[0];
        resolve([status, body]);
      });
    }
   if(type=='saleorder30')
   {
    let { response } = await soapRequest({
      url: url,
      headers: sampleHeaders,
      xml: xml30
    });
    let { headers, body, statusCode30 } = response;

    parseString(body, function (err, result) {
      let status =
        result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
          "GetExportJobStatusResponse"
        ][0].Successful[0];
      resolve([status, body]);
    });
   }
   if(type=='inventorysnapshot')
   {
     let { response } = await soapRequest({
       url: url,
       headers: sampleHeaders,
       xml: xml
     });
     let { headers, body, statusCode } = response;
     parseString(body, function (err, result) {
       let status =
         result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
           "GetExportJobStatusResponse"
         ][0].Successful[0];
       resolve([status, body]);
     });
   }
  //New Changes xmlshipping45dayback
    if (type == 'shippingpackagetimeline_glaucus') {
      let { response } = await soapRequest({
        url: dataurl,
        headers: sampleHeaders,
        xml: xmlshipping
      });
      // console.log(response,'REPOJNSEEEEEE');
      let { headers, body, statusCode } = response;
      parseString(body, function (err, result) {
        let status =
          result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
            "GetExportJobStatusResponse"
          ][0].Successful[0];
        resolve([status, body]);
      });
    }
    //new...........................
    if (type == 'shippingpackagetimeline_glaucus_1') {
      let { response } = await soapRequest({
        url: dataurl,
        headers: sampleHeaders,
        xml: xmlshipping45dayback
      });
      // console.log(response,'REPOJNSEEEEEE');
      let { headers, body, statusCode } = response;
      parseString(body, function (err, result) {
        let status =
          result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
            "GetExportJobStatusResponse"
          ][0].Successful[0];
        resolve([status, body]);
      });
    }
    //.............................
    if (type == 'shippingpackagetimeline_TCNS_113') {
      let { response } = await soapRequest({
        url: dataurlTCNS_113,
        headers: sampleHeaders,
        xml: xmlshipping
      });
      // console.log(response,'REPOJNSEEEEEE');
      let { headers, body, statusCode } = response;
      parseString(body, function (err, result) {
        let status =
          result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
            "GetExportJobStatusResponse"
          ][0].Successful[0];
        resolve([status, body]);
      });
    }
    //new...........................
    if (type == 'shippingpackagetimeline_TCNS_113_1') {
      let { response } = await soapRequest({
        url: dataurlTCNS_113,
        headers: sampleHeaders,
        xml: xmlshipping45dayback
      });
      // console.log(response,'REPOJNSEEEEEE');
      let { headers, body, statusCode } = response;
      parseString(body, function (err, result) {
        let status =
          result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
            "GetExportJobStatusResponse"
          ][0].Successful[0];
        resolve([status, body]);
      });
    }
    //.............................
    if (type == 'shippingpackagetimeline_GR_GL_01') {
      let { response } = await soapRequest({
        url: dataurlGR_GL_01,
        headers: sampleHeaders,
        xml: xmlshipping
      });
      // console.log(response,'REPOJNSEEEEEE');
      let { headers, body, statusCode } = response;
      parseString(body, function (err, result) {
        let status =
          result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
            "GetExportJobStatusResponse"
          ][0].Successful[0];
        resolve([status, body]);
      });
    }
    //new...........................
    if (type == 'shippingpackagetimeline_GR_GL_01_1') {
      let { response } = await soapRequest({
        url: dataurlGR_GL_01,
        headers: sampleHeaders,
        xml: xmlshipping45dayback
      });
      // console.log(response,'REPOJNSEEEEEE');
      let { headers, body, statusCode } = response;
      parseString(body, function (err, result) {
        let status =
          result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0][
            "GetExportJobStatusResponse"
          ][0].Successful[0];
        resolve([status, body]);
      });
    }
    //.............................
    //End New Changes 
  });

  return promise;
}

function save(filePath,type) {
  let promise = new Promise(async (resolve, reject) => {
    console.log("creating file");
    if(type=='shippingpackagetimeline_glaucus')
    {
      const file = fs.createWriteStream("shippingpackagetimeline_glaucus.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    //new........................
    if(type=='shippingpackagetimeline_glaucus_1')
    {
      const file = fs.createWriteStream("shippingpackagetimeline_glaucus_1.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    //............................
    else if(type=='shippingpackagetimeline_TCNS_113'){
      
      const file = fs.createWriteStream("shippingpackagetimeline_TCNS_113.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    //new........................
    else if(type=='shippingpackagetimeline_TCNS_113_1'){
      
      const file = fs.createWriteStream("shippingpackagetimeline_TCNS_113_1.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    //............................
    else if(type=='shippingpackagetimeline_GR_GL_01'){
      
      const file = fs.createWriteStream("shippingpackagetimeline_GR_GL_01.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
        //new........................
        else if(type=='shippingpackagetimeline_GR_GL_01_1'){
      
          const file = fs.createWriteStream("shippingpackagetimeline_GR_GL_01_1.csv");
          console.log("Saving");
          const request = http.get(filePath, response => {
            response.pipe(file);
          });
          file.on("finish", () => {
            resolve(request);
            console.log("saved");
          });
        }
    //............................
    //saleorder,reversepickup,saleorder30,inventorysnapshot
    else if(type=='saleorder'){
      
      const file = fs.createWriteStream("saleorder.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    else if(type=='saleorder1'){
      
      const file = fs.createWriteStream("saleorder1.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    else if(type=='reversepickup'){
      
      const file = fs.createWriteStream("reversepickup.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    else if(type=='saleorder30'){
      
      const file = fs.createWriteStream("saleorder30.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    //else if (type=='inventorysnapshot')
    else{
      
      const file = fs.createWriteStream("inventorysnapshot.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    // else{
    //   const file = fs.createWriteStream("file.csv");
    //   console.log("Saving");
    //   const request = http.get(filePath, response => {
    //     response.pipe(file);
    //   });
    //   file.on("finish", () => {
    //     resolve(request);
    //     console.log("saved");
    //   });
    // }
  
  });
  return promise;
}
function save30(filePath30,type) {
  let promise = new Promise(async (resolve, reject) => {
    console.log("creating file");
    if(type=='shippingpackagetimeline_glaucus')
    {
      const file = fs.createWriteStream("shippingpackagetimeline_glaucus.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    //new...................
    if(type=='shippingpackagetimeline_glaucus_1')
    {
      const file = fs.createWriteStream("shippingpackagetimeline_glaucus_1.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    //............................
    else if(type=='shippingpackagetimeline_TCNS_113'){
      
      const file = fs.createWriteStream("shippingpackagetimeline_TCNS_113.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
      //new...................
      else if(type=='shippingpackagetimeline_TCNS_113_1'){
      
        const file = fs.createWriteStream("shippingpackagetimeline_TCNS_113_1.csv");
        console.log("Saving");
        const request = http.get(filePath, response => {
          response.pipe(file);
        });
        file.on("finish", () => {
          resolve(request);
          console.log("saved");
        });
      }
    //............................
    else if(type=='shippingpackagetimeline_GR_GL_01'){
      
      const file = fs.createWriteStream("shippingpackagetimeline_GR_GL_01.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
      //new...................
      else if(type=='shippingpackagetimeline_GR_GL_01_1'){
      
        const file = fs.createWriteStream("shippingpackagetimeline_GR_GL_01_1.csv");
        console.log("Saving");
        const request = http.get(filePath, response => {
          response.pipe(file);
        });
        file.on("finish", () => {
          resolve(request);
          console.log("saved");
        });
      }
    //............................
    //saleorder,reversepickup,saleorder30,inventorysnapshot
    else if(type=='saleorder'){
      
      const file = fs.createWriteStream("saleorder.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    else if(type=='saleorder1'){
      
      const file = fs.createWriteStream("saleorder1.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    else if(type=='reversepickup'){
      
      const file = fs.createWriteStream("reversepickup.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    else if(type=='saleorder30'){
      
      const file = fs.createWriteStream("saleorder30.csv");
      console.log("Saving");
      const request = http.get(filePath30, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    //else if (type=='inventorysnapshot')
    else{
      
      const file = fs.createWriteStream("inventorysnapshot.csv");
      console.log("Saving");
      const request = http.get(filePath, response => {
        response.pipe(file);
      });
      file.on("finish", () => {
        resolve(request);
        console.log("saved");
      });
    }
    // else{
    //   const file = fs.createWriteStream("file.csv");
    //   console.log("Saving");
    //   const request = http.get(filePath, response => {
    //     response.pipe(file);
    //   });
    //   file.on("finish", () => {
    //     resolve(request);
    //     console.log("saved");
    //   });
    // }
  
  });
  return promise;
}

async function saveJson(json, type) {
  let promise = new Promise(async (resolve, reject) => {
    console.log("creating file");
    console.log(type, "type");

    const file = fs.appendFileSync(type + ".json", json);
    console.log("Saving");
    resolve(file);
    // const request = http.get(filePath, response => {
    //   response.pipe(file);
    // });
    // file.on("finish", () => {
    //   resolve(request);
    //   console.log('saved');

    // });
  });
  return promise;
}
async function saveXML(response, type) {
  let promise = new Promise(async (resolve, reject) => {
    console.log("creating file");
    console.log(type, "type");
    // require("fs").writeFile(
    //       "ecom.xml",
    //       response,
    //       function (err) { console.log(err ? 'Error :'+err : 'ok') }
    //  );
    try {
      response.shift();
      // response.unshift('<?xml version="1.0" encoding="utf-8"?>');

      const file = fs.writeFileSync("ecom.xml",
        response);
      console.log("Saving");
      const optionsXML = {

        //Single file
        files: './ecom.xml',

        //Replacement to make (string or regex) 
        from: /\,+\<+\?+[a-z ="0-9.-]+\?+\>/g,
        to: ' ',
      };
      await replace(optionsXML).then(changedFiles => {
        console.log('Modified files:', changedFiles.join(', '));
      }).catch(error => {
        console.error('Error occurred:', error);
      });
      blobName = "CloudStation/Ecom/ecom.xml";
      stream = fs.createReadStream("./ecom.xml");
      stats = fs.statSync("./ecom.xml");
      var fileSizeInBytes = stats["size"];
      console.log(fileSizeInBytes);

      //calling the upload method for azure blob
      let azureres = await azure.fileBlob(
        blobName,
        stream,
        fileSizeInBytes,
        _res => {
          console.log(_res);
        }
      );

      resolve(true)

    } catch (error) {
      console.log(error ? 'Error :' + error : 'ok')
      reject(false)
    }
    // const file = fs.writeFileSync("ecom.xml",
    //  response,
    //   function (err) 
    //   {
    //      console.log(err ? 'Error :'+err : 'ok') 
    //   });
    // console.log("Saving");
    // resolve(file);
    // const request = http.get(filePath, response => {
    //   response.pipe(file);
    // });
    // file.on("finish", () => {
    //   resolve(request);
    //   console.log('saved');

    // });

  });
  return promise;
}

async function xml2json(data) {
  let ecomArr = [];
  let promise = new Promise(async (resolve, reject) => {
    parseString(data, function (err, result) {
      //console.log(result,'result');
      data = result;
      resolve(data);
    });
  });
  return promise;
}

async function hitApi(url) {
  try {
    let promise = new Promise(async (resolve, reject) => {
      var response = await axios.get(url);
      resolve(response.data);
    });
    return promise;
  } catch (error) {
    console.log(error);
  }
}

async function makeRequest(type, arr) {
  console.log("inside make");
  console.log(arr.length);
  let connection = await sql.connect(_config.dbConfig);

  let responseArr = [];
  return new Promise(resolve => {
    let a = async () => {
      // arr.forEach(async (element,index)=>{
      for (let index = 0; index < arr.length; index++) {
        console.log(index);
        element = arr[index];
        let trackingNo = element.Tracking_Number;
        console.log(Number.parseFloat(trackingNo).toPrecision());
        trackingNo = Number.parseFloat(trackingNo).toPrecision();
        //console.log(delhiveryElement);
        if (type == "ecom") {
          var url =
            "https://plapi.ecomexpress.in/track_me/api/mawbd/?password=WJu47WgNg5mBR35K&username=tcnsclothing112200_women&awb=" +
            trackingNo;
        } else {
          var url =
            "https://track.delhivery.com/api/packages/json/?token=713c3dba49be9d786774b45235e1f17bf9973a0e&waybill=" +
            trackingNo;
        }
        console.log(url);
        try {
          let res = await axios.get(url);

          var data = res.data;
          //console.log();

          //console.log("ecom response");
          if (type == "ecom") {
            var resp = data; //await xml2json(data)
            let result = await connection
      .request().input('trackingNo', sql.NVarChar, trackingNo).input('resp', sql.NVarChar, resp).input('batchid', sql.NVarChar, batchid)
      .query(
        "INSERT INTO temp_ecom_and_scan_stages1 (awb_number, xml_response, batchid) VALUES (@trackingNo ,@resp, @batchid)"
      );
      console.log(result,"Insert Result");
      
          } else {
            var resp = data;
          }
          // console.log(resp, "ecom res");
          responseArr.push(resp);

          if (element.Reverse_Tracking_Number) {
            trackingNo = Number.parseFloat(trackingNo).toPrecision();
            let res = await axios.get(url);

            var data = res.data;
            //console.log();

            //console.log("ecom response");
            // if(type == 'ecom'){
            //   var resp = data //await xml2json(data)

            // }else{
            //   var resp = data
            // }
            var resp = data;
            // console.log(resp, type + "res");
            responseArr.push(resp);
          }
        } catch (error) {
          //console.log(response.data);

          // finalEcomArray.push(res);

          console.log(error, "error in process arrays");
        }
      }
      return responseArr;
    };
    resolve(a());
  });
}

//   let promise = new Promise(async (resolve, reject) => {
// console.log('processing arrays',arr.length);

//   let a1 = arr.slice(0,Math.trunc((arr.length/10)*100)).forEach(async (element,index)=>{
//     console.log(index);
//       element = arr[index];
//       let trackingNo = element.Tracking_Number;
//        console.log(Number.parseFloat(trackingNo).toPrecision());
//        trackingNo = Number.parseFloat(trackingNo).toPrecision();
//        //console.log(delhiveryElement);
//        var url = "https://plapi.ecomexpress.in/track_me/api/mawbd/?password=WJu47WgNg5mBR35K&username=tcnsclothing112200_women&awb="+trackingNo;
//        console.log(url);
//          try {
//            var response = await axios.get(url);
//            //console.log(response.data);

//            var data = response.data;
//            console.log();

//            console.log("ecom response");
//            let res = await xml2json(data)
//           // finalEcomArray.push(res);
//           console.log(res);

//            await saveJson(JSON.stringify(res),type)

//           }catch(error){
//             console.log(error,"error in process arrays");
//           }
//   });
//   let a2 = arr.slice(Math.trunc((arr.length/10)*100),Math.trunc((arr.length/20)*100)).forEach(async (element,index)=>{
//     console.log(index,'2');
//       element = arr[index];
//       let trackingNo = element.Tracking_Number;
//        console.log(Number.parseFloat(trackingNo).toPrecision());
//        trackingNo = Number.parseFloat(trackingNo).toPrecision();
//        //console.log(delhiveryElement);
//        var url = "https://plapi.ecomexpress.in/track_me/api/mawbd/?password=WJu47WgNg5mBR35K&username=tcnsclothing112200_women&awb="+trackingNo;
//        console.log(url);
//          try {
//            var response = await axios.get(url);
//            //console.log(response.data);

//            var data = response.data;
//            console.log();

//            console.log("ecom response");
//            let res = await xml2json(data)
//           // finalEcomArray.push(res);
//           console.log(res);

//            await saveJson(JSON.stringify(res),type)

//           }catch(error){
//             console.log(error,"error in process arrays");
//           }
//   });

//resolve(a1,a2)

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
async function process(arrayOfPromises) {
  //console.time(`process`);
  let responses = await Promise.all(arrayOfPromises);
  for (let r of responses) {
  }
  //console.timeEnd(`process`);
  return;
}
